#ifndef PLAYER_H
#define PLAYER_H

#include <ostream>
#include <string>
#include <map>
#include "json.hpp"
#include "Item.h"

enum Base {
	AP = 160
};
namespace Race{
	enum Race : int {
		ORC = 2,
		UNDEAD = 5,
		TAUREN = 6,
		TROLL = 8
	};
}
namespace Str{
	enum RaceStr : int {
		UNDEAD = 119,
		TROLL = 121,
		ORC = 123,
		TAUREN = 125
	};
}
namespace Agi{
	enum RaceAgi : int {
		TAUREN = 75,
		ORC = 77,
		UNDEAD = 78,
		TROLL = 82,
	};
}

class Player {
public:
	Player();
	static const std::map<std::string, std::pair<int,int>> baseStats;
	
	std::string name;
	std::string race;
	int level;
	Stats stats;
	
	void fromData(std::string name, std::string race, int str, int agi, int ap, int critChance, int hitChance, int weaponskill);
	
	int totalAp() const;
	double totalCrit() const;
	
	Player operator+(const Item&) const;
	Player operator-(const Item&) const;
	friend std::ostream& operator<< (std::ostream&, const Player&);
	friend void from_json(const nlohmann::json&, Player&);
};

class Target {
public:
	Target();
	
	unsigned int resistance;
	int level;
	double armor, health;
	double parryChance, dodgeChance, blockChance;
};

class Buffs {
public:
	Buffs();
	friend void from_json(const nlohmann::json&, Buffs&);

	bool onyBuff, songflowerBuff, rendBuff, zgBuff, dmAPBuff;
	bool strTotem, agiTotem, enhaInGroup, windfuryTotem, motw, trueshotAura;
	bool faerieFire, curseOfRecklessness, annihilator;
	bool mongoose, blessedSunFruit, sandwormMeat, giants;
	
	
};

class Talents {
public:
	Talents();
	friend void from_json(const nlohmann::json&, Talents&);
	
	int improvedBattleShout, improvedHeroicStrike, improvedExecute, unbridledWrath, impale, cruelty;
};

class Options {
public:
	Options();
	friend void from_json(const nlohmann::json&, Options&);
	
	bool tanking, use_hamstring, RageofMugamba, BloodGuardsPlateGloves, HandofJustice, DarkmoonCardMaelstrom;
	double hamstring_swing_limit;
	int HeroicStrikeMinRage;
	
};


#endif /* PLAYER_H */

