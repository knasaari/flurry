#ifndef BRAWL_H
#define BRAWL_H

#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <future>
#include "Skill.h"
#include "Player.h"
#include "Rage.h"
#include "Outcome.h"

struct Rolls {
	int missYellow, missYellowNoWeapon, miss,
	skillDifference, skillDifferenceNoBenefit,
	dodge, parry, glancing, crit, spellCrit, block, resist,
	targetDefence;
	double glancingMultiplier, impaleMultiplier, resistanceChance, targetArmor;
};

class Brawl {
private:
	std::unordered_map<unsigned int, Weaponproc> weaponProcList;

	unsigned int fps;
	int seconds;
	unsigned int step;

	int ap;
	Rolls rolls;
	Rage rage;

	unsigned int rollSpell(unsigned int);
	unsigned int rollMelee(unsigned int);
	double calculateDamage(Weapon*, Skill*, unsigned int);
	void updateRolls(const Player&, const Target&, const Stats&, const Talents&, const Options&, bool);
	Stats getStats(const Weapon&, const Weapon&, const Player&, bool, const Buffs&, const Talents&) const;
	void addWeaponProc(Weaponproc&);
	void addWeaponProc(Weaponproc&, unsigned int);
public:
	void run(unsigned int iterations, Weapon mainhandWeapon, Weapon offhandWeapon, bool dualwielding, const Player&, const Target&, Buffs&, Talents&, Options&, Simulation*, bool = true);
	void help(int);
	Brawl();
};

#endif /* BRAWL_H */

