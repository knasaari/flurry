#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <utility>
#include <vector>
#include <map>
#include <iostream>
#include "json.hpp"

class Stats {
public:
	int str, agi, ap, weaponskill, armorPenetration;
	double critChance, hitChance, spellCritChance;
	
	Stats();
	/**
	 * 
	 * @param str
	 * @param agi
	 * @param ap
	 * @param crit
	 * @param hit
	 * @param weaponskill
	 * @param spell
	 * @param armorPenetration
	 */
	Stats(int str,int agi,int ap, double crit, double hit, int weaponskill, double spell = 0.0, int armorPenetration = 0);
	int totalAp() const;
	double totalCrit() const;
	Stats& operator+=(const Stats&);
	Stats operator+(const Stats&) const;
	Stats operator-(const Stats&) const;
	Stats operator-() const;
	friend std::ostream& operator<< (std::ostream&, const Stats&);
	friend void from_json(const nlohmann::json&, Stats&);
	friend void to_json(nlohmann::json&, const Stats&);
};

class Set {
private:
	public:
	std::string name;
	std::vector<std::pair<int, Stats>> bonuses;
	unsigned int id;

	Set();
	Set(const std::string&, unsigned int);
	void addBonus(int, const Stats&);
	Stats getStats(int) const;
	bool operator<(const Set &other) const;
	friend void from_json(const nlohmann::json&, Set&);
	friend void to_json(nlohmann::json&, const Set&);
};

class Item {
public:
	Stats stats;
	std::string name;
	std::map<Set, int> sets;
	
	typedef void is_transparent;
	
	Item();
	/**
	 * 
	 * @param str
	 * @param agi
	 * @param ap
	 * @param crit
	 * @param hit
	 * @param weaponskill
	 * @param spell
	 * @param armorPenetration
	 */
	Item(const std::string&, const Stats&);
	Item(int str,int agi,int ap, double crit, double hit, int weaponskill, double spell = 0.0, int armorPenetration = 0);
	
	void addSet(const Set&, int = 1);
	void removeSet(const Set&, int = 1);
	Stats getSetBonuses() const;
	bool hasSetBonuses() const;
	Item& operator+=(const Item &item);
	Item operator+(const Item &item) const;
	Item operator-(const Item &item) const;
	Item operator-() const;
	bool operator<(const Item&) const;
	bool operator()(const Item&, const Item&) const;
	bool operator()(const Item&, const std::string&) const;
	bool operator()(const std::string&, const Item&) const;
	
	friend std::ostream& operator<< (std::ostream&, const Item&);
	friend void from_json(const nlohmann::json&, Item&);
	friend void to_json(nlohmann::json&, const Item&);
};

class Outfit {
public:
	Outfit();
	
	std::string name;
	std::vector<std::string> items;
	friend void from_json(const nlohmann::json&, Outfit&);
};
#endif /* ITEM_H */

