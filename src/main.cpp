#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <random>
#include <functional>
#include <chrono>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <thread> 
#include <future>
#include "json.hpp"

#include "Brawl.h"
#include "Item.h"
#include "Outcome.h"
#include "functions.h"

std::mt19937 rng;
std::set<Item, Item> items;

Item findItem(std::string);

using HRClock = std::chrono::high_resolution_clock;

int main(int argc, char** argv) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	rng = std::mt19937(seed);
	
	std::map<std::string, Player> players;
	std::map<std::string, Outfit> outfits;
	std::vector<Weapon> weapons;
	bool dualwielding = false;
	Weapon mainhandWeapon, offhandWeapon;
	Player player;
	Outfit outfit;
	Buffs buffs;
	Talents talents;
	Options options;
	Brawl program;
	
	std::ifstream i("players.json");
	try{
		nlohmann::json j;
		i >> j;
		buffs = j["buffs"];
		talents = j["talents"];
		options = j["options"];
		for (nlohmann::json::iterator it = j["players"].begin(); it != j["players"].end(); ++it) {
			Player p = *it;
			players.insert(std::pair<const char*, Player>(p.name.c_str(), p));
		}
	} catch (nlohmann::detail::exception e){
		std::cout << "players.json is broken: " << std::endl << e.what() << std::endl;
		return 0;
	}catch (std::invalid_argument e){
		std::cout << "players.json: " << e.what() << std::endl;
		return 0;
	}
	i.close();
	
	i.open("items.json");
	try{
		nlohmann::json j;
		i >> j;
		std::set<Item, Item> itemsz = j["items"];
		std::vector<Weapon> weaponsz = j["weapons"];
		items = itemsz;
		weapons = weaponsz;
	} catch (nlohmann::detail::exception e){
		std::cout << "items.json is broken: " << std::endl << e.what() << std::endl;
		return 0;
	}
	i.close();

	i.open("outfits.json");
	try{
		nlohmann::json j;
		i >> j;
		for (nlohmann::json::iterator it = j.begin(); it != j.end(); ++it) {
			Outfit o = *it;
			outfits.insert(std::pair<const char*, Outfit>(o.name.c_str(), o));
		}
	} catch (nlohmann::detail::exception e){
		std::cout << "outfits.json is broken: " << std::endl << e.what() << std::endl;
		return 0;
	}
	i.close();
	
	if (argc == 1) {
		program.help(weapons.size()-1);
		return 0;
	}
	else if (argc > 1) {
		if (strcmp(argv[1], "help") == 0) {
			program.help(weapons.size()-1);
			return 0;
		}
		else if (strcmp(argv[1], "weapons") == 0) {
			for (int i = 0; i < weapons.size(); i++) {
				std::cout << i << " - " << weapons[i].item.name << std::endl;
			}
			return 0;
		}
		else if (strcmp(argv[1], "weapon") == 0) {
			if(argc < 3){
				return 0;
			}
			
			for (int i = 0; i < weapons.size(); i++) {
				if(weapons[i].item.name.compare(argv[2]) == 0){
					std::cout << weapons[i] << std::endl;
					return 0;
				}
			}
			
			std::cout << "Could not find weapon '" << argv[2] << "' in items.json." << std::endl;
			return 0;
		}
		else if (strcmp(argv[1], "version") == 0) {
			std::cout << "Version: " << VERSION << std::endl;
			return 0;
		}
		else if (strcmp(argv[1], "players") == 0) {
			std::cout << "Available Player templates:" << std::endl;
			for(std::map<std::string, Player>::iterator it=players.begin(); it!=players.end(); ++it){
				std::cout << it->first << std::endl;
			}
			return 0;
		}
		else if (strcmp(argv[1], "items") == 0) {
			std::cout << "Available items:" << std::endl;
			for(std::set<Item, Item>::iterator it=items.begin(); it!=items.end(); ++it){
				std::cout << it->name << std::endl;
			}
			return 0;
		}
		else if (strcmp(argv[1], "item") == 0) {
			if(argc < 3){
				return 0;
			}
			
			try{
				Item search = findItem(argv[2]);
				std::cout << search << std::endl;
			} catch(int e){
			}
			return 0;
		}
		else if (strcmp(argv[1], "outfits") == 0) {
			std::cout << "Available outfits:" << std::endl;
			for(auto it = outfits.begin(); it!=outfits.end(); ++it){
				std::cout << it->first << std::endl;
			}
			return 0;
		}
		else if (strcmp(argv[1], "dumpweapons") == 0) {
			std::ofstream ou("weapons.json");
			try{
				nlohmann::json j;
				j = weapons;
				ou << std::setw(4) << j;
				
			} catch (nlohmann::detail::exception e){
				std::cout << e.what() << std::endl;
			}
			ou.close();
			return 0;
		}
		else if (strcmp(argv[1], "secret") == 0) {
//			int rounds = 100;
//			int tally = 0;
//			unsigned int resistance = 300;
//			for(int i=0;i<rounds;i++){
//				int damage = getPartialResistReducedDamage(getSpellResistChance(63,60,resistance,true), 1500);
//				std::cout << damage << std::endl;
//				tally += damage;
//			}
//			std::cout << tally/rounds << std::endl;
			double row[4];
			std::cout << "resistance;75%;50%;25%;0%" << std::endl;
			int resistances[] = {0,10,20,30,40,50,60,70,80,90,100,120,150,200,300,315,330};
			std::vector<int> values(resistances, resistances + sizeof(resistances) / sizeof(int));
			for(int i=0;i<values.size();i++){
				getPartialResistReducedDamageTable(getSpellResistChance(63, 60, values[i], false), row);
				std::cout << values[i] << ";" << row[0] << ";" << row[1] << ";" << row[2] << ";" << row[3] << std::endl;
				//std::cout << i << " - " << (double)(10000 - getMagicSpellHitChance(60, 63, true, i))/100.0 << std::endl;
				//std::cout << i << " - " << getSpellResistChance(60,63,i,false) << std::endl;
			}
			return 0;
		}
		
		// Mainhand weapon
		std::istringstream ssm(argv[1]);
		int main = 0;
		ssm >> main;
		if (main < 0 || main >= weapons.size()) {
			main = 0;
		}
		
		mainhandWeapon = weapons[main];
	}
	
	// Offhand weapon
	if (argc > 2) {
		int off = -1;
		std::istringstream ss(argv[2]);
		ss >> off;
		if (off < 0 || off >= weapons.size()) {
			off = -1;
		}
		else{
			dualwielding = true;
			offhandWeapon = weapons[off];
		}
	}
	
	// Player
	if (argc > 3) {
		std::string playerSelector;
		std::istringstream ss(argv[3]);
		ss >> playerSelector;
		if(players.find(playerSelector.c_str()) != players.end()){
			player = players.at(playerSelector.c_str());
		}
		else{
			std::cout << "Player '" << playerSelector << "' not defined in players.json" << std::endl;
			return 0;
		}
	}
	
	// Outfit
	if (argc > 4) {
		std::string outfitSelector;
		std::istringstream ss(argv[4]);
		ss >> outfitSelector;
		if(outfits.find(outfitSelector.c_str()) != outfits.end()){
			outfit = outfits.at(outfitSelector.c_str());
			Item items;
			try{
				for(int i=0; i<outfit.items.size(); i++){
					items += findItem(outfit.items[i]);
				}
			} catch (int e){
				return 0;
			}
			
			options.RageofMugamba = (items.name.find("Rage of Mugamba") != std::string::npos);
			options.BloodGuardsPlateGloves = (items.name.find("Blood Guard's Plate Gauntlets") != std::string::npos || items.name.find("General's Plate Gauntlets") != std::string::npos);
			options.HandofJustice = (items.name.find("Hand of Justice") != std::string::npos);
			options.DarkmoonCardMaelstrom = (items.name.find("Darkmoon Card: Maelstrom") != std::string::npos);
			player = player + items;
		}
		else{
			std::cout << "Outfit '" << outfitSelector << "' not defined in outfits.json" << std::endl;
			return 0;
		}
	}
	
	Target target;
	target.dodgeChance = 5;
	target.parryChance = 12.5;
	target.blockChance = 0;
	target.level = 63;
	target.health = 150000;
	target.armor = 3731;	// Usual boss armor in MC
	target.resistance = 15; // Who knows what resistances bosses have?

	int threadCount = 8;
	int iterations = 50000/8;
	std::vector<std::thread> threads;
	std::vector<Simulation> sims2(threadCount, Simulation());
	std::cout << "Running " << iterations*threadCount << " simulations using " << threadCount << " threads." << std::endl;
	HRClock::time_point start = HRClock::now();
	
	for(int i=0;i<threadCount;i++){
		threads.push_back(std::thread(std::bind(&Brawl::run, program, iterations, mainhandWeapon, offhandWeapon, dualwielding, player, target, buffs, talents, options, sims2.data()+i, true)));
	}
	Outcome combined;
	double totalSeconds = 0;
	for(int i=0;i<threads.size();i++){
		threads[i].join();
		Simulation &sim = sims2[i];
		//std::cout << sim.data.totalDPS(sim.seconds) << " " << sim.seconds <<std::endl;
		combined += sim.data;
		totalSeconds += sim.seconds;
	}
	combined.setSpellName("Total");
	HRClock::time_point end = HRClock::now();
	double seconds = std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();
	std::cout << std::endl << combined.damageInfo(threadCount * iterations, totalSeconds);
	std::cout << std::endl << "Running simulations took "<< seconds << "s" <<std::endl;
	
	return 0;
}

Item findItem(std::string item){
	std::set<Item,Item>::iterator it = items.find(item);
	if(it == items.end()){
		std::cout << "Could not find '" << item << "' in items.json." << std::endl;
		throw 1;
	}
	return *it;
}
