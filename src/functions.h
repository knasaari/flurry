#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#include <random>
extern std::mt19937 rng;

inline double getApBonus(int ap, double speed){return (double)ap / 14.0 * speed;}
double getApBonusNormalized(int ap, unsigned int weaponType);
int getArmorReducedDamage(double, int, int);
double getMissChance(int skillDifference);
double getGlancingReduceMultiplierLow(int skillDifference);
double getGlancingReduceMultiplierHigh(int skillDifference);
double getGlancingReduceMultiplier(int skillDifference);
double rageGained(int damage, int level);
inline double agiToCrit(int agility){return (double) agility / 20.0;}
inline int maxSkillValueForLevel(int level){return level * 5;}
int getPartialResistReducedDamage(double resistanceChance, int damage);
void getPartialResistReducedDamageTable(double resistanceChance, double *row);
double getSpellResistChance(int casterLevel, int targetLevel, unsigned int targetResistance, bool innateResists);
int getMagicSpellHitChance(int casterLevel, int targetLevel, bool binary = false, unsigned int targetResistance = 0);

enum HitType : unsigned int {
	MISS		= 0x1,
	DODGE		= 0x2,
	PARRY		= 0x4,
	BLOCK		= 0x8,
	GLANCING	= 0x10,
	CRIT		= 0x20,
	HIT			= 0x40,
	CONNECTED	= GLANCING | CRIT | HIT | BLOCK,
	WHITEATTACK	= 0x80,
	YELLOWATTACK= 0x100,
	BENEFITS_FROM_WEAPONSKILL = 0x200,
	OFFHAND		= 0x400,
	MAINHAND	= 0x100000,
	ANYHAND		= MAINHAND | OFFHAND,
	GCD			= 0x800,
	IMPALE_MOD	= 0x1000,
	PHYSICAL	= 0x2000,
	FIRE		= 0x4000,
	NATURE		= 0x8000,
	FROST		= 0x10000,
	SHADOW		= 0x20000,
	ARCANE		= 0x40000,
	SPELL		= FIRE | NATURE | FROST | SHADOW | ARCANE,
	RESIST		= 0x80000,
	SWING		= 0x200000,
	EXECUTE		= 0x400000,
	EXTRAATTACK	= 0x800000,
	WINDFURY	= 0x1000000 | EXTRAATTACK,
	SKILL		= 0x2000000,
	BUFF		= 0x4000000,
	DEBUFF		= 0x8000000
};

enum WeaponType : unsigned int{
	DAGGER		= 1,
	ONEHANDED	= 2,
	TWOHANDED	= 3,
	RANGED		= 4,
};

struct ResistanceValues {
	int resist100;
	int resist75;
	int resist50;
	int resist25;
	int resist0;
	unsigned int chanceResist;
};

#endif