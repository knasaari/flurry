#ifndef WEAPON_H
#define WEAPON_H

#include <string>
#include <random>
#include "Item.h"
#include "json.hpp"

class Weapon {
public:
	double speed;
	int low, high;
	unsigned int weaponType, procSpellId;
	Item item;
	
	Weapon();
	Weapon(std::string name, unsigned int weaponType, double speed, int low, int high, int str, int agi, int ap, int crit, int hit, int weaponskill);
	Weapon(std::string name, unsigned int weaponType, double speed, int low, int high, int str, int agi, int ap, int crit, int hit, int weaponskill, unsigned int procSpellId);

	double getAverageDamage(bool, int);
	int getDamage(bool, int, bool = false);
	int getLowEnd(bool, int);
	int getHighEnd(bool, int);
	double getDPS(bool, int);
	friend std::ostream& operator<< (std::ostream&, const Weapon&);
	friend void from_json(const nlohmann::json&, Weapon&);
	friend void to_json(nlohmann::json&, const Weapon&);
};

#endif /* WEAPON_H */

