#include "functions.h"
#include <algorithm>

double getApBonusNormalized(int ap, unsigned int weaponType){
	double speed = 2.4;
	switch(weaponType){
		case DAGGER:
			speed = 1.7;
			break;
		case ONEHANDED:
			speed = 2.4;
			break;
		case TWOHANDED:
			speed = 3.3;
			break;
		case RANGED:
			speed = 2.8;
	}
	return (double)ap / 14.0 * speed;
}

int getArmorReducedDamage(double armor, int damage, int playerLevel) {
	if (damage < 1)
		return 0;

	int newdamage = 0;

	if (armor < 0.0)
		armor = 0.0;

	double tmpvalue = 0.1 * armor / (8.5 * (double) playerLevel + 40);
	tmpvalue = tmpvalue / (1.0 + tmpvalue);

	if (tmpvalue < 0.0)
		tmpvalue = 0.0;
	if (tmpvalue > 0.75)
		tmpvalue = 0.75;

	newdamage = int(damage - (damage * tmpvalue));

	return (newdamage > 1) ? newdamage : 1;
}

double getMissChance(int skillDifference){
	double missChance = 0.0;
	
	if (skillDifference > 10){
		missChance = 5 + ((skillDifference) * 0.2);
	}
	else{
		missChance = 5 + (skillDifference * 0.1);
	}
	
	if(missChance < 0 ){
		return 0.0;
	}
	if(missChance > 60.0){
		return 60.0;
	}
	
	return missChance;
}

double getGlancingReduceMultiplierLow(int skillDifference) {
	// 1.3 - 0.05*(defense-skill) capped at 0.91

	return std::min(1.3 - 0.05 * skillDifference, 0.91);
}

double getGlancingReduceMultiplierHigh(int skillDifference) {
	// 1.2 - 0.03*(defense-skill) min of 0.2 and capped at 0.99

	return std::max(std::min(1.2 - 0.03 * skillDifference, 0.99), 0.2);
}

double getGlancingReduceMultiplier(int skillDifference) {
	return (getGlancingReduceMultiplierLow(skillDifference) + getGlancingReduceMultiplierHigh(skillDifference)) * 0.5;
}

double rageGained(int damage, int level){
	double conversion = 0.0091107836 * (double)level * (double)level + 3.225598133 * (double)level + 4.2652911;
	return damage / conversion * 7.5;
}

static ResistanceValues resistValues[] = {
	{0, 0, 0, 0, 100, 0}, // 0
	{0, 0, 2, 6, 92, 3}, // 10
	{0, 1, 4, 12, 84, 5}, // 20
	{0, 1, 5, 18, 76, 8}, // 30
	{0, 1, 7, 23, 69, 10}, // 40
	{0, 2, 9, 28, 61, 13}, // 50
	{0, 2, 11, 33, 54, 15}, // 60
	{0, 2, 13, 37, 37, 18}, // 70
	{0, 3, 15, 41, 41, 20}, // 80
	{1, 3, 17, 46, 36, 23}, // 90
	{1, 4, 19, 47, 29, 25}, // 100
	{1, 5, 21, 48, 24, 28}, // 110
	{1, 6, 24, 49, 20, 30}, // 120
	{1, 8, 28, 47, 17, 33}, // 130
	{1, 9, 33, 43, 14, 35}, // 140
	{1, 11, 37, 39, 12, 38}, // 150
	{1, 13, 41, 35, 10, 40}, // 160
	{1, 16, 45, 30, 8, 43}, // 170
	{1, 18, 48, 26, 7, 45}, // 180
	{2, 20, 48, 24, 6, 48}, // 190
	{4, 23, 48, 21, 4, 50}, // 200
	{5, 25, 47, 19, 3, 53}, // 210
	{7, 28, 45, 17, 2, 55}, // 220
	{9, 31, 43, 16, 2, 58}, // 230
	{11, 34, 40, 14, 1, 60}, // 240
	{13, 37, 37, 12, 1, 62}, // 250
	{15, 41, 33, 10, 1, 65}, // 260
	{18, 44, 29, 8, 1, 68}, // 270
	{20, 48, 25, 7, 1, 70}, // 280
	{23, 51, 20, 5, 1, 73}, // 290
	{25, 55, 16, 3, 1, 75} // 300
};

/**
 * Adapted from Light's Hope January 2018
 * 
 * @param resistanceChance
 * @param damage
 * @return 
 */
int getPartialResistReducedDamage(double resistanceChance, int damage){
	bool canResist = true;
	int resist = 0;
	
	if (canResist)
    {
        //float resistanceChance = getSpellResistChance(this, schoolMask, true);
        resistanceChance *= 100.0;

        // DoTs
        // The mechanic for this is strange in classic - most dots can be seen exhibiting partial resists in videos, but only very rarely,
        // and almost never more than 25% resists. How this should work exactly is somewhat a guess.
        // Kalgan post-2.0 dot nerf: "Previously, dots in general were 1/10th as likely to be resisted as normal spells."
        // http://web.archive.org/web/20080601184008/http://forums.worldofwarcraft.com/thread.html?topicId=65457765&pageNo=18&sid=1#348
        /*
		if (damagetype == DOT && spellProto)
        {
            switch (spellProto->Id)
            {
                // NOSTALRIUS: Some DoTs follow normal resist rules. Need to find which ones, why and how.
                // We have a video proof for the following ones.
                case 23461:     // Vaelastrasz's Flame Breath
                case 24818:     // Nightmare Dragon's Noxious Breath
                case 25812:     // Lord Kri's Toxic Volley
                case 28531:     // Sapphiron's Frost Aura
                    break;
                default:
                    resistanceChance *= 0.1f;
            }
        }
		*/
        ResistanceValues* prev = nullptr;
        ResistanceValues* next = nullptr;
        for (int i = 1; i < 31; ++i)
        {
            // On depasse la valeur cherchee.
            if (resistValues[i].chanceResist >= resistanceChance)
            {
                prev = &resistValues[i - 1];
                next = &resistValues[i];
                break;
            }
        }
		
        double coeff = double(resistanceChance - prev->chanceResist) / double(next->chanceResist - prev->chanceResist);
        double resist0 = prev->resist0 + (next->resist0 - prev->resist0) * coeff;
        double resist25 = prev->resist25 + (next->resist25 - prev->resist25) * coeff;
        double resist50 = prev->resist50 + (next->resist50 - prev->resist50) * coeff;
        double resist75 = prev->resist75 + (next->resist75 - prev->resist75) * coeff;
        double resist100 = prev->resist100 + (next->resist100 - prev->resist100) * coeff;
		
		std::uniform_int_distribution<unsigned int> distr(0, 99);
        unsigned int ran = distr(rng);
        
		double resistCnt = 0.0;
        // Players CANNOT resist 100% of damage, it is always rounded down to 75%, despite what Blizzard's table sugests.
        // The true magic damage resist cap is therefore actually ~68-70% because of this mechanic.
        // http://web.archive.org/web/20110808083353/http://elitistjerks.com/f15/t10712-resisting_magical_damage_its_relation_resistance_levels/p4/
        if (ran < resist100 + resist75)
            resistCnt = 0.75;
        else if (ran < resist100 + resist75 + resist50)
            resistCnt = 0.5;
        else if (ran < resist100 + resist75 + resist50 + resist25)
            resistCnt = 0.25;

		//std::cout << "Partial resist : chances " << resist0 << ":" << resist25 << ":" << resist50 << ":" << resist75 << ":%" << resist100<< ". Hit resist chance " << resistanceChance << std::endl;
      
        resist += (int)(damage * resistCnt);
        if (resist > damage)
            resist = damage;
    }
    else
        resist = 0;
	
	return damage - resist;
}

void getPartialResistReducedDamageTable(double resistanceChance, double *row){
	resistanceChance *= 100.0;

	ResistanceValues* prev = nullptr;
	ResistanceValues* next = nullptr;
	for (int i = 1; i < 31; ++i)
	{
		// On depasse la valeur cherchee.
		if (resistValues[i].chanceResist >= resistanceChance)
		{
			prev = &resistValues[i - 1];
			next = &resistValues[i];
			break;
		}
	}

	double coeff = double(resistanceChance - prev->chanceResist) / double(next->chanceResist - prev->chanceResist);
	double resist0 = prev->resist0 + (next->resist0 - prev->resist0) * coeff;
	double resist25 = prev->resist25 + (next->resist25 - prev->resist25) * coeff;
	double resist50 = prev->resist50 + (next->resist50 - prev->resist50) * coeff;
	double resist75 = prev->resist75 + (next->resist75 - prev->resist75) * coeff;
	double resist100 = prev->resist100 + (next->resist100 - prev->resist100) * coeff;

	std::uniform_int_distribution<unsigned int> distr(0, 99);
	unsigned int ran = distr(rng);

	// Players CANNOT resist 100% of damage, it is always rounded down to 75%, despite what Blizzard's table sugests.
	// The true magic damage resist cap is therefore actually ~68-70% because of this mechanic.
	// http://web.archive.org/web/20110808083353/http://elitistjerks.com/f15/t10712-resisting_magical_damage_its_relation_resistance_levels/p4/
	row[0] = resist100 + resist75;
	row[1] = resist100 + resist75 + resist50 - row[0];
	row[2] = resist100 + resist75 + resist50 + resist25 - row[0] - row[1];
	row[3] = 100.0 - (row[0] + row[1] + row[2]);
}

/**
 * Adapted from Light's Hope January 2018
 * 
 * @param casterLevel
 * @param targetLevel
 * @param schoolMask
 * @param innateResists
 * @return 
 */
double getSpellResistChance(int casterLevel, int targetLevel, unsigned int targetResistance, bool innateResists) {
    // Get base victim resistance for school
    double resistModHitChance = targetResistance;
    // Ignore resistance by self SPELL_AURA_MOD_TARGET_RESISTANCE aura
    //resistModHitChance += GetTotalAuraModifierByMiscMask(SPELL_AURA_MOD_TARGET_RESISTANCE, schoolMask); <-- don't need spell penetration for Warrior simulation -Kazar
    // No "negative" resist chance. Applied before innate resists.
    if (resistModHitChance < 0)
        resistModHitChance = 0.0;

    // Computing innate resists, resistance bonus when attacking a creature higher level. Not affected by modifiers.
    if (innateResists /*&& victim->GetTypeId() == TYPEID_UNIT*/)
    {
        int leveldiff = targetLevel - casterLevel;
        resistModHitChance += int((8.0 * leveldiff) * casterLevel / 63.0);
    }
    resistModHitChance *= (double)(0.15 / casterLevel);

    if (resistModHitChance < 0.0)
        resistModHitChance = 0.0;
    if (resistModHitChance > 0.75)
        resistModHitChance = 0.75;

    return resistModHitChance;
}

int getMagicSpellHitChance(int casterLevel, int targetLevel, bool binary, unsigned int targetResistance){
    // PvP - PvE spell misschances per leveldif > 2
    //int32 lchance = pVictim->GetTypeId() == TYPEID_PLAYER ? 7 : 11;
    int lchance = 11;
    int leveldif = targetLevel - casterLevel;

    // Base hit chance from attacker and victim levels
    double modHitChance;
    if (leveldif < 3)
        modHitChance = 96 - leveldif;
    else
        modHitChance = 94 - (leveldif - 2) * lchance;

    // Spellmod from SPELLMOD_RESIST_MISS_CHANCE
//    if (Player *modOwner = GetSpellModOwner())
//    {
//        modOwner->ApplySpellMod(spell->Id, SPELLMOD_RESIST_MISS_CHANCE, modHitChance, spellPtr);
//        DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "SPELLMOD_RESIST_MISS_CHANCE : %f", modHitChance);
//    }
	
    // Chance hit from victim SPELL_AURA_MOD_ATTACKER_SPELL_HIT_CHANCE auras
//    modHitChance += pVictim->GetTotalAuraModifierByMiscMask(SPELL_AURA_MOD_ATTACKER_SPELL_HIT_CHANCE, schoolMask);
//    DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "SPELL_AURA_MOD_ATTACKER_SPELL_HIT_CHANCE (+ %i) : %f", pVictim->GetTotalAuraModifierByMiscMask(SPELL_AURA_MOD_ATTACKER_SPELL_HIT_CHANCE, schoolMask), modHitChance);

    // Reduce spell hit chance for Area of effect spells from victim SPELL_AURA_MOD_AOE_AVOIDANCE aura
//    if (IsAreaOfEffectSpell(spell))
//    {
//        modHitChance -= pVictim->GetTotalAuraModifier(SPELL_AURA_MOD_AOE_AVOIDANCE);
//        DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "SPELL_AURA_MOD_AOE_AVOIDANCE (- %i) : %f", pVictim->GetTotalAuraModifier(SPELL_AURA_MOD_AOE_AVOIDANCE), modHitChance);
//    }

    // Chance resist mechanic for spell (effect resistance handled later)
//    int32 resist_mech = 0;
//    if (spell->Mechanic)
//        resist_mech = pVictim->GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_MECHANIC_RESISTANCE, spell->Mechanic);
    // Apply mod
//    modHitChance -= resist_mech;
//    DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "SPELL_AURA_MOD_MECHANIC_RESISTANCE (- %i) : %f", resist_mech, modHitChance);

    // Chance resist debuff
//    modHitChance -= pVictim->GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_DEBUFF_RESISTANCE, int32(spell->Dispel));
//    DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "SPELL_AURA_MOD_DEBUFF_RESISTANCE (- %i) : %f", pVictim->GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_DEBUFF_RESISTANCE, int32(spell->Dispel)), modHitChance);

    // Increase hit chance from attacker SPELL_AURA_MOD_SPELL_HIT_CHANCE and attacker ratings
//    modHitChance += int32(m_modSpellHitChance);
//    DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "SPELL_AURA_MOD_SPELL_HIT_CHANCE (+ %i) : %f", int32(m_modSpellHitChance), modHitChance);

    // Nostalrius: sorts binaires.
    if (binary){
        // Get base victim resistance for school
        float resistModHitChance = getSpellResistChance(casterLevel, targetLevel, targetResistance, false);
        modHitChance *= (1 - resistModHitChance);
        //DEBUG_UNIT(this, DEBUG_SPELL_COMPUTE_RESISTS, "x %f : HitChance = %f", (1 - resistModHitChance), modHitChance);
    }

    int HitChance = modHitChance * 100;
    if (HitChance <  100) HitChance =  100;
    if (HitChance > 9900) HitChance = 9900;
    return HitChance;
}
