#include "Weapon.h"
#include "functions.h"

Weapon::Weapon() {
	item.name = "Unknown weapon";
	speed = 1.0;
	low = 10;
	high = 20;
	weaponType = ONEHANDED;
	procSpellId = 0;
}

/**
 * 
 * @param name
 * @param speed
 * @param low
 * @param high
 * @param str
 * @param agi
 * @param ap
 * @param crit
 * @param hit
 * @param weaponskill
 */
Weapon::Weapon(std::string name, unsigned int weaponType, double speed, int low, int high, int str, int agi, int ap, int crit, int hit, int weaponskill) :
item(name, Stats(str, agi, ap, crit, hit, weaponskill)) {
	this->speed = speed;
	this->low = low;
	this->high = high;
	this->weaponType = weaponType;
	procSpellId = 0;
}

Weapon::Weapon(std::string name, unsigned int weaponType, double speed, int low, int high, int str, int agi, int ap, int crit, int hit, int weaponskill, unsigned int procSpellId):
Weapon(name, weaponType, speed, low, high, str, agi, ap, crit, hit, weaponskill){
	this->procSpellId = procSpellId;
}

double Weapon::getAverageDamage(bool mainhand, int ap){
	return ((double)(high+low)/2.0) * ((mainhand)?1:0.75) + getApBonus(ap, speed) * ((mainhand)?1:0.5);
}

int Weapon::getDamage(bool mainhand, int ap, bool normalized){
	int apbonus = (normalized)? getApBonusNormalized(ap, weaponType) : getApBonus(ap, speed);
	if(!mainhand){
		apbonus /=2;
	}
	
	int lowEnd = low * ((mainhand)?1:0.75) + apbonus;
	int highEnd = high * ((mainhand)?1:0.75) + apbonus;
	
	std::uniform_int_distribution<int> distr(lowEnd, highEnd);
	
	return distr(rng);
}

int Weapon::getLowEnd(bool mainhand, int ap){
	int apbonus = (int)getApBonus(ap, speed);
	if(!mainhand){
		apbonus /=2;
	}
	int lowEnd = low * ((mainhand)?1:0.75) + apbonus;
	
	return lowEnd;
}

int Weapon::getHighEnd(bool mainhand, int ap){
	int apbonus = (int)getApBonus(ap, speed);
	if(!mainhand){
		apbonus /=2;
	}
	int highEnd = high * ((mainhand)?1:0.75) + apbonus;
	
	return highEnd;
}

double Weapon::getDPS(bool mainhand, int ap){
	double damage = this->getAverageDamage(mainhand, ap);
	return damage/this->speed;
}

std::ostream& operator<<(std::ostream& stream, const Weapon& weapon){
	stream << weapon.item << "\n\nDamage:\t" << weapon.low << "-" << weapon.high << "\nSpeed:\t" << weapon.speed << "\nProc:\t" << weapon.procSpellId;

	return stream;
}

void from_json(const nlohmann::json& j, Weapon& weapon){
	Weapon temp;
	temp.high = j.at("high");
	temp.low = j.at("low");
	temp.item = j.at("item");
	temp.procSpellId = j.at("procSpellId");
	temp.speed = j.at("speed");
	temp.weaponType = j.at("weaponType");
	weapon = temp;
}

void to_json(nlohmann::json& j, const Weapon& weapon){
	nlohmann::json temp;
	temp["high"] = weapon.high;
	temp["low"] = weapon.low;
	temp["item"] = weapon.item;
	temp["procSpellId"] = weapon.procSpellId;
	temp["speed"] = weapon.speed;
	temp["weaponType"] = weapon.weaponType;
	
	j = temp;
}
