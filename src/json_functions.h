#ifndef JSON_FUNCTIONS_H
#define JSON_FUNCTIONS_H

#include <string>
#include "json.hpp"	

void assign_if_exists(std::string &value, const nlohmann::json &json_object, const std::string& key);
void assign_if_exists(int &value, const nlohmann::json &json_object, const std::string& key);
void assign_if_exists(double &value, const nlohmann::json &json_object, const std::string& key);

#endif /* JSON_FUNCTIONS_H */

