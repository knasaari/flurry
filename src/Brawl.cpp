#include "Brawl.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <numeric>
#include <stack>
#include <algorithm>
#include <cmath>
#include "functions.h"

Brawl::Brawl() {
	fps = 50;
	seconds = 600;
	step = 1000 / fps; // 50ms intervals

	GenericSkill* Firebolt = new GenericSkill(23267, 40, 57, HitType::FIRE);
	GenericSkill* FireboltAlcor = new GenericSkill(18833, 75, 106, HitType::FIRE);
	GenericSkill* Thunderfury = new GenericSkill(21992, 300, 300, HitType::NATURE);
	GenericSkill* FatalWound = new GenericSkill(21140, 240, 240, PHYSICAL | YELLOWATTACK);
	GenericSkill* ShadowBolt = new GenericSkill(18138, 110, 141, HitType::SHADOW);
	GenericSkill* DrainLife = new GenericSkill(24585, 48, 55, HitType::SHADOW);
	GenericSkill* Fireball = new GenericSkill(21162, 273, 334, HitType::FIRE);
	GenericSkill* Kalimdor = new GenericSkill(26415, 239, 277, HitType::NATURE);
	GenericSkill* TeebusFirebolt = new GenericSkill(18086, 150, 150, HitType::FIRE);
	GenericSkill* Wrath = new GenericSkill(18104, 90, 126, HitType::NATURE);
	ExtraAutoattack* Thrash = new ExtraAutoattack(true);
	Thrash->setSpellId(21919);
	ExtraAutoattack* FuryofForgewright = new ExtraAutoattack(true);
	FuryofForgewright->setSpellId(15494);
	
	GenericBuff* Felstriker = new GenericBuff(16551, 0, 3000, Stats(0,0,0,100.0,100.0,0,0,0));
	GenericBuff* Untamed = new GenericBuff(23719, 0, 8000, Stats(300,0,0,0,0,0,0,0));
	GenericBuff* BonereaversEdge = new GenericBuff(21153, 0, 10000, Stats(0,0,0,0,0,0,0,700));
	GenericBuff* ArmorShatter = new GenericBuff(16928, HitType::SHADOW|DEBUFF, 45000, Stats(0,0,0,0,0,0,0,200));
	
	Weaponproc Draketalon_proc(1, true, SKILL, FatalWound, "Fwnd"); // assumed
	Weaponproc Viskag_proc(0.6, true, SKILL, FatalWound, "Fwnd");
	Weaponproc Perdition_proc(1, true, SKILL, Firebolt, "Fib"); // assumed
	Weaponproc Thunderfury_proc(7.89473684210526, true, SKILL, Thunderfury, "TF");
	Weaponproc Alcor_proc(1, true, SKILL, FireboltAlcor, "FibA"); // assumed
	Weaponproc Deathbringer_proc(0.8, true, SKILL, ShadowBolt, "Shb");
	Weaponproc Thrashblade_proc(1, true, SKILL, Thrash, "Thr");
	Weaponproc Manslayer_proc(1, true, SKILL, DrainLife, "DrLi"); // assumed
	Weaponproc Ironfoe_proc(0.8, true, SKILL, FuryofForgewright, "IF", 2);
	Weaponproc Sulfuras_proc(0.7, true, SKILL, Fireball, "Sulf");
	Weaponproc Kalimdor_proc(1, true, SKILL, Kalimdor, "Shock"); // assumed
	Weaponproc Teebus_proc(1, true, SKILL, TeebusFirebolt, "FibT"); // assumed
	Weaponproc DeepWoods_proc(0.8, true, SKILL, Wrath, "Wrath");
	Weaponproc Felstriker_proc(1, true, BUFF, Felstriker, "Felstriker");
	Weaponproc Untamed_proc(1, true, BUFF, Untamed, "Untamed Fury"); // assumed
	Weaponproc Bonereaver_proc(2, true, BUFF, BonereaversEdge, "Bonereaver's Edge");
	Weaponproc Annihilator_proc(2, true, SKILL, ArmorShatter, "Armor Shatter");
	
	addWeaponProc(Draketalon_proc, 21141);
	addWeaponProc(Viskag_proc);
	addWeaponProc(Perdition_proc);
	addWeaponProc(Thunderfury_proc);
	addWeaponProc(Alcor_proc);
	addWeaponProc(Deathbringer_proc);
	addWeaponProc(Thrashblade_proc);
	addWeaponProc(Manslayer_proc);
	addWeaponProc(Ironfoe_proc);
	addWeaponProc(Sulfuras_proc);
	addWeaponProc(Kalimdor_proc);
	addWeaponProc(Teebus_proc);
	addWeaponProc(DeepWoods_proc);
	addWeaponProc(Felstriker_proc);
	addWeaponProc(Untamed_proc);
	addWeaponProc(Bonereaver_proc);
	addWeaponProc(Annihilator_proc);
}

unsigned int Brawl::rollSpell(unsigned int flags) {
	int roll = rng() % 10000;
	int sum = rolls.resist;
	if (roll < sum) {
		// miss
		return RESIST;
	}
	
	sum += rolls.spellCrit;
	if (roll < sum) {
		// crit
		return CRIT;
	}

	return HIT;
}

unsigned int Brawl::rollMelee(unsigned int flags) {
	int roll = rng() % 10000;

	int sum = 0;
	if(flags & YELLOWATTACK){
		if(flags & BENEFITS_FROM_WEAPONSKILL)
			sum = rolls.missYellow;
		else
			sum = rolls.missYellowNoWeapon;
	}
	else{
		sum = rolls.miss;
	}
	
	if (roll < sum) {
		// miss
		return MISS;
	}

	int skillBonus = 10 * -((flags & BENEFITS_FROM_WEAPONSKILL) ? rolls.skillDifference : rolls.skillDifferenceNoBenefit);
	int tmp = rolls.dodge;
	if ((tmp > 0) && ((tmp -= skillBonus) > 0) && roll < (sum += tmp)) {
		// dodge
		return DODGE;
	}
	
	tmp = rolls.parry;
	if ((tmp > 0) && ((tmp -= skillBonus) > 0) && roll < (sum += tmp)) {
		// parry
		return PARRY;
	}

	tmp = rolls.glancing;
	if ((flags & WHITEATTACK) && roll < (sum += tmp)) {
		// glance
		return GLANCING;
	}

//	tmp = rolls.block;
//	if ((tmp > 0) && roll < (sum += tmp)) {
//		if(rng() % 10000 < rolls.crit)
//			return CRIT;
//		// block
//		return BLOCK;
//	}
	
	sum += rolls.crit;
	if (roll < sum) {
		// crit
		return CRIT;
	}

	return HIT;
}

double Brawl::calculateDamage(Weapon* weapon, Skill* skill, unsigned int flags) {
	double damage = 0;
	if(flags & EXECUTE){
		double tmp = std::floor(rage.current());
		damage = skill->getDamage(weapon, tmp);
		rage.spend(tmp);
	}
	else{
		damage = skill->getDamage(weapon, this->ap);
	}

	if (flags & GLANCING) {
		damage *= rolls.glancingMultiplier;
	}
	else if (flags & CRIT) {
		double multiplier = (flags & SPELL)?1.5:2.0;
		if (flags & IMPALE_MOD) {
			multiplier += rolls.impaleMultiplier;
			
		}
		damage *= multiplier;
	}

	return damage;
}

void Brawl::run(unsigned int iterations, Weapon mainhandWeapon, Weapon offhandWeapon, bool dualwielding, const Player &player, const Target &target, Buffs &buffs, Talents &talents, Options &options, Simulation *results, bool print) {
	std::vector<Weaponproc> weaponProcs;
	int loopLimit = fps * seconds;
	
	Stats staticStats = getStats(mainhandWeapon, offhandWeapon, player, dualwielding, buffs, talents);
	updateRolls(player, target, staticStats, talents, options, dualwielding);
	Stats stats = staticStats;
	
	double attackSpeedModifier = (buffs.rendBuff)?1.15:1.0;
	
	std::stack<Skill*> stack;
	std::unordered_map<unsigned int, Outcome> statistics;
	std::unordered_map<int, GenericBuff> activeBuffs;
	std::unordered_map<unsigned int, GenericBuff> activeDebuffs;
	std::unordered_map<int, unsigned int> buffUptimeTicks;
	std::unordered_map<unsigned int, unsigned int> debuffUptimeTicks;
	
	Autoattack mainhand(mainhandWeapon, true);
	mainhand.setFlags(mainhand.getFlags() | MAINHAND);
	statistics[mainhand.getSpellId()] = Outcome("MainH");
	
	Autoattack offhand(offhandWeapon, false);
	offhand.setSpellId(2);
	offhand.setFlags(offhand.getFlags() | OFFHAND);
	statistics[offhand.getSpellId()] = Outcome("OffH");

	Bloodthirst bloodthirst;
	statistics[bloodthirst.getSpellId()] = Outcome("BT");
	
	Whirlwind whirlwind;
	statistics[whirlwind.getSpellId()] = Outcome("WW");
	
	int hamstringReduce = 0;
	hamstringReduce += (options.RageofMugamba)?2:0;
	hamstringReduce += (options.BloodGuardsPlateGloves)?3:0;
	Hamstring hamstring(hamstringReduce);
	if(options.use_hamstring){
		statistics[hamstring.getSpellId()] = Outcome("Ham");
	}
	
	HeroicStrike heroicstrike(talents.improvedHeroicStrike);
	statistics[heroicstrike.getSpellId()] = Outcome("HS");
	int heroicStrikeThreshold = std::max(heroicstrike.getRageCost(), options.HeroicStrikeMinRage);
	
	Execute execute(talents.improvedExecute);
	statistics[execute.getSpellId()] = Outcome("EXE");
	
	GenericBuff crusader = GenericBuff(20007, 0, 15000, Stats(100,0,0,0,0,0,0,0));
	GenericBuff crusader2 = crusader;
	Weaponproc crusaderMH(1, true, BUFF|MAINHAND, &crusader, "Crusader MH");
	Weaponproc crusaderOH(1, true, BUFF|OFFHAND, &crusader2, "Crusader OH");
	weaponProcs.push_back(crusaderMH);
	weaponProcs.push_back(crusaderOH);

	Flurry flurry;
	GlobalCD gcd;

	ExtraAutoattack extraMainhand(true);
	extraMainhand.setFlags(extraMainhand.getFlags() | MAINHAND | EXTRAATTACK);
	
	WindfuryAttack windfury(true, buffs.enhaInGroup);
	windfury.setFlags(windfury.getFlags()| MAINHAND | WINDFURY);
	
	GenericSkill lightningStrike(23686, 200, 301, NATURE);
	
	Weaponproc WindfuryProc(20, false, SKILL|MAINHAND, &windfury, "WF"); // 20%
	Weaponproc HOJ(2, false, SKILL|ANYHAND, &extraMainhand, "HOJ"); // 2%
	Weaponproc Maelstrom(1, true, SKILL|ANYHAND, &lightningStrike, "DC:M"); // 1 ppm?
	
	double unbridledChance = (talents.unbridledWrath >= 1 && talents.unbridledWrath <= 5)?8.0 * talents.unbridledWrath:0;
	Weaponproc UnbridledWrath(unbridledChance, false, ANYHAND, nullptr, "UW");
	
	if(buffs.windfuryTotem){
		weaponProcs.push_back(WindfuryProc);
	}
	
	if(options.HandofJustice){
		weaponProcs.push_back(HOJ);
	}
	
	if(options.DarkmoonCardMaelstrom){
		weaponProcs.push_back(Maelstrom);
	}
	
	std::unordered_map<unsigned int, Weaponproc>::iterator it;
	if((it = weaponProcList.find(mainhandWeapon.procSpellId)) != weaponProcList.end()){
		Weaponproc proc = it->second;
		proc.addFlags(MAINHAND);
		weaponProcs.push_back(proc);
	}
	
	if(dualwielding && (it = weaponProcList.find(offhandWeapon.procSpellId)) != weaponProcList.end()){
		Weaponproc proc = it->second;
		proc.addFlags(OFFHAND);
		weaponProcs.push_back(proc);
	}
	
	for(int i=0;i<weaponProcs.size();i++){
		if(weaponProcs[i].getFlags() & SKILL){
			Skill *skill = weaponProcs[i].getSkill();
			statistics[skill->getSpellId()] = Outcome(weaponProcs[i].getName());
		}
		else if(weaponProcs[i].getFlags() & BUFF){
			buffUptimeTicks[i] = 0;
		}
		else if(weaponProcs[i].getSkill()->getFlags() & DEBUFF){
			debuffUptimeTicks[weaponProcs[i].getSkill()->getSpellId()] = 0;
		}
	}
	
	int flurryUp = 0;
	unsigned int totalLoops = 0;
	rage = Rage();
	ap = stats.totalAp();
	
	double hamstringLimit = std::min(std::max(options.hamstring_swing_limit, 0.0), 1.0);
//	std::stringstream combatLog;
//	combatLog << std::setprecision(2) << std::fixed;
	
	if(print){
		std::cout << "Simulating " << iterations << " fights. " << std::endl <<
			"Player: " << player << std::endl << std::flush;
	}
	
	for (int sims = 0; sims < iterations; sims++) {
		mainhand.reset();
		offhand.reset();
		mainhand.prime();
		offhand.prime();
		
		whirlwind.prime();
		hamstring.prime();
		bloodthirst.prime();
		gcd.prime();
		flurry.prime();

		double bossHealth = target.health;
		double executeThreshold = bossHealth * 0.2;
		
		rage.reset();
		//rage.add(10); // Simulated start rage from Bloodrage

		bool statsChanged = false;
		int i = 0;
		while(bossHealth > 0 && i < loopLimit) {
			Stats activeBuffStats;
			gcd.update(step);
			flurry.update(step);
			if (flurry.isUp())
				flurryUp++;
			
			auto itb = activeBuffs.begin();
			while(itb != activeBuffs.end()){
				GenericBuff& s = (itb->second);
				if(s.update(step)){
					// Buff expired
					buffUptimeTicks[itb->first] += s.getUptime();
					itb = activeBuffs.erase(itb);
					statsChanged = true;
				}
				else{
					++itb;
				}
			}
			
			auto itd = activeDebuffs.begin();
			while(itd != activeDebuffs.end()){
				GenericBuff& s = (itd->second);
				if(s.update(step)){
					// Buff expired
					debuffUptimeTicks[itd->first] += s.getUptime();
					itd = activeDebuffs.erase(itd);
					statsChanged = true;
				}
				else{
					++itd;
				}
			}
			
			if(statsChanged){
				itb = activeBuffs.begin();
				while(itb != activeBuffs.end()){
					activeBuffStats += itb->second.getStats();
					++itb;
				}
				
				itd = activeDebuffs.begin();
				while(itd != activeDebuffs.end()){
					activeBuffStats += itd->second.getStats();
					++itd;
				}
				
				stats = staticStats + activeBuffStats;
				updateRolls(player, target, stats, talents, options, dualwielding);
				ap = stats.totalAp();
				statsChanged = false;
			}
//			if(options.Bonereaver){
//				bonereaver.update(step);
//				if(bonereaver.isUp())
//					breUp++;
//				double bonereaveArp = 700.0 * bonereaver.getCount();
//				targetArmorWithArp = std::max(rolls.targetArmor - bonereaveArp, 0.0);
//			}
			whirlwind.update(step);
			bloodthirst.update(step);
			mainhand.update(step);
			if(dualwielding)
				offhand.update(step);
			
			if(bossHealth > executeThreshold){
				//if(options.use_hamstring && !(bloodthirst.cooldownLeft() < 0.5 && rage.current() >= bloodthirst.getRageCost())){
				if(options.use_hamstring && mainhand.getSwingPct() < hamstringLimit){
					stack.push(&hamstring);
				}
				if(rage.current() > 50 || bloodthirst.cooldownLeft() >= 2000)
					stack.push(&whirlwind);
				stack.push(&bloodthirst);
			}
			else{
				stack.push(&execute);
			}
			
			if(dualwielding)
				stack.push(&offhand);
			stack.push(&mainhand);
			
			while(!stack.empty()){
				Skill *skill = stack.top();
				stack.pop();
				unsigned int flag = skill->getFlags();
				unsigned int spellId = skill->getSpellId();
				if (skill->ready()) {
					if(flag & GCD && !gcd.ready() || rage.current() < skill->getRageCost()){
						continue;
					}
					
					// Use GCD
					if (flag & GCD){
						gcd.reset();
					}
					
					rage.spend(skill->getRageCost());
					
					// Reset skill internal cooldown
					if (flag & YELLOWATTACK) {
						skill->reset();
					}

					if (flag & SWING && flag & WHITEATTACK && !(flag & WINDFURY)) {
						flurry.reduce();
						
						if(flag & MAINHAND && rage.current() >= heroicStrikeThreshold){
							stack.push(&heroicstrike);
							continue;
						}
					}
					
					// Throw dice and decide what happened
					unsigned int outcome = (flag & SPELL)?rollSpell(flag):rollMelee(flag);
					if (outcome & CONNECTED) {
						if (outcome & CRIT && !(flag & SPELL)) {
							flurry.reset();
						}

						double damage = calculateDamage(&mainhandWeapon, skill, outcome | flag);
						if (!(flag & SPELL)) {
							// Armor damage reduction
							damage = getArmorReducedDamage(rolls.targetArmor, damage, player.level);
						}
						else{
							if(flag & DEBUFF){
								auto debuff = activeDebuffs.find(spellId);
								if(debuff == activeDebuffs.end()){
									statsChanged = true;
									activeDebuffs[spellId] = *dynamic_cast<GenericBuff*>(skill);
									activeDebuffs[spellId].reset();
								}
								else{
									debuff->second.reset();
								}
							}
							// Partial resists
							damage = getPartialResistReducedDamage(rolls.resistanceChance, damage);
						}
						
//						if(outcome & BLOCK){
//							std::cout << "blocked " << damage << std::endl;
//						}
						
						statistics[spellId].add(outcome, damage);
						bossHealth -= damage;
						
						//statistics[spellId].output(combatLog, outcome, damage, i);
						
						if (flag & WHITEATTACK) {
							rage.add(rageGained(damage, player.level));
							
							if(UnbridledWrath.roll(0)){
								rage.add(1);
							}
						}
						
						// Weaponprocs
						if(flag & ANYHAND){
							for(int p=0; p<weaponProcs.size(); p++){
								Weaponproc& proc = weaponProcs[p];
								unsigned int procFlags = proc.getFlags();
								
								// Make sure mainhand only procs mainhand and offhand procs offhand
								if(flag & MAINHAND & procFlags ||
									flag & OFFHAND & procFlags){
								
									double speed = (flag & MAINHAND)?mainhandWeapon.speed:offhandWeapon.speed;
									Skill *s = proc.getSkill();
									
									if(s->getSpellId() == spellId){
										// Prevent proc from proccing itself
										continue;
									}
									
									if(proc.roll(speed)){
										if(procFlags & SKILL){
											s->prime();
											for(int prcs=0;prcs<=proc.getMultiproc();prcs++)
												stack.push(s);
										}
										else if(procFlags & BUFF){
											if(activeBuffs.find(p) == activeBuffs.end()){
												statsChanged = true;
												activeBuffs[p] = *dynamic_cast<GenericBuff*>(s);
											}
											else{
												activeBuffs[p].reset();
											}
										}
									}
								}
							}
						}
					}
					else{
						statistics[spellId].add(outcome, 0);
						//statistics[spellId].output(combatLog, outcome, 0, i);
						if(outcome & (PARRY | DODGE)){
							if (flag & WHITEATTACK) {
								// Parried or dodged white attacks give 75% of rage normal swing would have given on unarmored enemy
								double damage = calculateDamage(&mainhandWeapon, skill, 0);
								rage.add(rageGained(damage, player.level) * 0.75);
							}
							else if(flag & YELLOWATTACK && spellId != whirlwind.getSpellId()){
								// Warriors and Druids are refunded 80% of the rage cost on dodge/parry
								// http://blue.mmo-champion.com/topic/69365-18-02-05-kalgans-response-to-warriors/
								// https://youtu.be/YzPlictRoK8?t=1m45s
								// Kazar: For some reason its set to 82% on Lights Hope
								//combatLog << "Refunded " << 0.82 * skill->getRageCost() << " rage." << std::endl;
								rage.refund(0.82 * skill->getRageCost());
							}
						}
					}
					
					// Extra attacks reset mainhand swing timer. Heroic strike mh reset.
					if(flag & EXTRAATTACK || (flag & SWING && flag & YELLOWATTACK)){
						mainhand.reset(flurry.isUp()?attackSpeedModifier + 0.3:attackSpeedModifier);
					}
					else if (flag & SWING && flag & WHITEATTACK) {
						skill->reset(flurry.isUp()?attackSpeedModifier + 0.3:attackSpeedModifier);
					}
					
					if(bossHealth < 0)
						break;
				}
			}
			i++;
		}
//		double dps = targetDefaultHealth / ((double)i/fps);
//		combatLog << dps << std::endl;
		totalLoops += i;
	}
	
//	std::ofstream myfile;
//	myfile.open ("combatlog.txt");
//	if(myfile.is_open()){
//		myfile << combatLog.rdbuf();
//		myfile.close();
//	}
	
	
	std::ostringstream os, os2, breSs;
	os << std::setprecision(2) << std::fixed;

	double totalSeconds = (double)totalLoops/fps;
	bool first = true;
	
	Outcome all;
	for (auto it = statistics.begin(); it!=statistics.end(); ++it){
		all += it->second;
		
		if(first)
			os << it->second.header() << std::endl;
		
		os << it->second << std::endl;
		
		os2 << it->second.damageInfo(iterations, totalSeconds, first) << std::endl;
		first = false;
	}
	all.setSpellName("Total");
	
	*results = Simulation(totalSeconds, all);
	
	if(print){
		double avgMinute = (totalSeconds/iterations)/60.0;
		std::string ohString = (dualwielding)?"Offhand: "+ offhandWeapon.item.name +"\n":"";
		std::cout << std::setprecision(2) << std::fixed <<
			"Buffed stats: str " << staticStats.str << ", agi " << staticStats.agi << ", ap " << staticStats.totalAp() << ", crit " << staticStats.totalCrit() << "%" << " weaponskill: " << staticStats.weaponskill << std::endl <<
			"Target lvl: " << target.level << ", armor: " << target.armor << ", armor after debuffs: " << rolls.targetArmor << ", mitigation: " << (10000-getArmorReducedDamage(rolls.targetArmor, 10000, 60)) << std::endl <<
			"Average duration " << totalSeconds/iterations << " seconds." << std::endl << std::endl <<
			"Mainhand: " << mainhandWeapon.item.name << std::endl <<
			ohString << std::endl <<
			os.str() << std::endl <<
			os2.str() <<
			all.damageInfo(iterations, totalSeconds) << std::endl << std::endl <<
			"Flurry uptime: " << (double) flurryUp / totalLoops * 100.0 << "%, procs avg: " << (double) flurry.procCount / iterations << std::endl <<
			breSs.str() << 
			"Rage gain/s: " << rage.gained / totalSeconds << ", spent/s: " << rage.used / totalSeconds << ", wasted: " << rage.wasted / iterations << ", unbridled procs: " << UnbridledWrath.getProcs() / iterations << std::endl <<
			std::endl;
		
		for(int p=0; p<weaponProcs.size(); p++){
			if(weaponProcs[p].getFlags() & BUFF)
				std::cout << weaponProcs[p].getName() << " uptime: " << (double)buffUptimeTicks[p]/totalLoops*100.0 << "%" << std::endl;
			else if(weaponProcs[p].getSkill()->getFlags() & DEBUFF)
				std::cout << weaponProcs[p].getName() << " uptime: " << (double)debuffUptimeTicks[weaponProcs[p].getSkill()->getSpellId()]/totalLoops*100.0 << "%" << std::endl;
		}
	}
}

Stats Brawl::getStats(const Weapon &mainhand, const Weapon &offhand, const Player &player, bool dualwielding, const Buffs &buffs, const Talents &talents) const{
	Stats stats;

	stats = player.stats;

	int mongooseAgi = 25;
	int totemStr = 70; // Rank 4
	int totemAgi = 77;
	int sunfruitStr = 10;
	int sandwormStr = 20;
	
	stats.critChance += 3.0; // Berserker stance
	
	stats.critChance += (talents.cruelty >= 1 && talents.cruelty <= 5)?talents.cruelty:0;
	
	//battleshout
	int impBs = (talents.improvedBattleShout >= 1 && talents.improvedBattleShout <= 5)?talents.improvedBattleShout:0;
	stats.ap += 193 * (1.0 + impBs * 0.05); // With 5/5 talent giving 25% bonus ap
	
	// Rallying Cry of the Dragonslayer
	if(buffs.onyBuff){
		stats.ap += 140;
		stats.critChance += 5.0;
		stats.spellCritChance += 10.0;
	}

	if(buffs.songflowerBuff){
		// songflower
		stats.critChance += 5;
		stats.spellCritChance += 5;
		stats.agi += 15;
		stats.str += 15;
	}

	if(buffs.mongoose){
		// mongoose
		stats.critChance += 2.0;
		stats.agi += mongooseAgi;
	}

	if(buffs.giants){
		stats.str += 25;
	}
	
	if(buffs.strTotem){
		stats.str += (buffs.enhaInGroup)? totemStr * 1.15 : totemStr;
	}
	
	if(buffs.agiTotem){
		stats.agi += ((buffs.enhaInGroup)? totemAgi * 1.15 : totemAgi);
	}
	
	if(buffs.motw){
		stats.agi += 16;
		stats.str += 16;
	}
	
	if(buffs.trueshotAura){
		stats.ap += 100;
	}
	
	if(buffs.blessedSunFruit){
		stats.str += sunfruitStr;
	}
	
	if(buffs.sandwormMeat){
		stats.str += sandwormStr;
	}
	
	if(buffs.dmAPBuff){
		stats.ap += 200;
	}
	
	Item weapons = mainhand.item;
	if (dualwielding) {
		weapons = weapons + offhand.item;
	}
	
	stats = stats + weapons.stats + weapons.getSetBonuses();

	if(buffs.zgBuff){
		stats.str *= 1.15;
		stats.agi *= 1.15;
	}
	
	int sunderStacks = 5;

	stats.armorPenetration += sunderStacks * 450;
	
	if(buffs.faerieFire){
		stats.armorPenetration += std::round(505);
	}
	
	if(buffs.curseOfRecklessness){
		stats.armorPenetration += std::round(640);
	}
	
	if(buffs.annihilator){
		stats.armorPenetration += std::round(600);
	}

	return stats;
}

void Brawl::updateRolls(const Player &player, const Target &target, const Stats &stats, const Talents &talents, const Options &options, bool dualwielding) {
	int maxSkillValue_player = maxSkillValueForLevel(player.level);
	
	rolls.targetDefence = maxSkillValueForLevel(target.level);
	
	double gearHit = stats.hitChance;
	if((rolls.targetDefence - (stats.weaponskill + maxSkillValue_player)) > 10){
		gearHit = (gearHit - 1.0 < 0)?0:gearHit - 1.0;
	}
	
	double misschance = getMissChance(rolls.targetDefence - (stats.weaponskill + maxSkillValue_player));
	double misschanceYellow = std::max(misschance - gearHit, 0.0);
	
	if(dualwielding){
		misschance = misschance * 0.8 + 20.0;
	}
	misschance = std::max(misschance - gearHit, 0.0);
	
	rolls.targetArmor = std::max(target.armor - stats.armorPenetration, 0.0);
	
	rolls.skillDifferenceNoBenefit = rolls.targetDefence - maxSkillValue_player;
	rolls.skillDifference = rolls.skillDifferenceNoBenefit + stats.weaponskill;

	rolls.glancingMultiplier = getGlancingReduceMultiplier(rolls.skillDifference);
	rolls.resistanceChance = getSpellResistChance(player.level, target.level, target.resistance, true);
	
	rolls.dodge = target.dodgeChance * 100;
	if(options.tanking){
		if(target.level - player.level == 3)
			rolls.parry = 12.5 * 100.0;
		else if(target.level - player.level == 2)
			rolls.parry = 5.5 * 100.0;
		else if(target.level - player.level == 1)
			rolls.parry = 4.5 * 100.0;
		else
			rolls.parry = 3.5 * 100.0;
		
		rolls.block = 500;
	}
	else{
		rolls.block = 0;
		rolls.parry = 0;
	}
	
	rolls.miss = misschance * 100;
	rolls.missYellow = misschanceYellow * 100;
	rolls.missYellowNoWeapon = (getMissChance(player.level * 5 - rolls.targetDefence) - stats.hitChance) * 100;
	if(rolls.missYellowNoWeapon < 0)
		rolls.missYellowNoWeapon = 0;
	
	rolls.glancing = (10 + ((rolls.targetDefence - player.level * 5) * 2)) * 100;
	rolls.glancing = std::min(std::max(rolls.glancing,0),4000);
	
	double crit = stats.critChance;
	// 1.8% crit aura suppression for +3 targets
	if(target.level - player.level > 2){
		crit -= 1.8;
	}
	crit += agiToCrit(stats.agi);
	
	// Higher level target
	if(rolls.skillDifferenceNoBenefit > 0){
		crit += (maxSkillValue_player - rolls.targetDefence) * 0.2;
	}
	// Equal level target or below
	else{
		crit += (maxSkillValue_player - rolls.targetDefence) * 0.04;
	}
	
	rolls.crit = std::max(crit, 0.0) * 100;
	rolls.spellCrit = stats.spellCritChance * 100;
	
	rolls.resist = 10000 - getMagicSpellHitChance(player.level, target.level);
	rolls.impaleMultiplier = (talents.impale >= 0 && talents.impale < 3)? 0.1 * talents.impale : 0;
}

void Brawl::help(int weaponsSize) {
	std::cout << "Usage: flurry [MAINHAND] [OFFHAND] [PLAYER] [OUTFIT]" << std::endl <<
		"Where MAINHAND and OFFHAND are from range -1 .. " << weaponsSize << ". Setting offhand -1 will disable it." << std::endl <<
		"PLAYER is a player profile in players.json" << std::endl <<
		"OUTFIT is a collection of items in outfits.json" << std::endl << std::endl <<
		"Examples:" << std::endl <<
		"\tflurry 0 2 Lyenache prebis\tsimulates Lyenache with Deathbinger, Crul'shorukh and prebis outfit" << std::endl <<
		"\tflurry 12 -1 Kazar fightclub\tsimulates Kazar with Bonereaver's Edge and fightclub outfit" << std::endl << std::endl <<

		"Miscellaneous:" << std::endl <<
		"\tweapons\t\tlists available weapons" << std::endl <<
		"\tweapon \"name\"\tdisplay weapon stats" << std::endl <<
		"\tplayers\t\tlists available player templates" << std::endl <<
		"\toutfits\t\tlists available outfits" << std::endl <<
		"\titems\t\tlists available items usable in the outfits" << std::endl <<
		"\titem \"name\"\tdisplay item stats" << std::endl <<
		"\tversion\t\tdisplays short git version hash and this file's build date" << std::endl <<
		"\thelp\t\tdisplays this help text" << std::endl << std::endl <<
		"You can edit items.json, players.json, and outfits.json to add new items and such." << std::endl;
}

void Brawl::addWeaponProc(Weaponproc& proc){
	weaponProcList.insert(std::pair<unsigned int, Weaponproc>(proc.getSkill()->getSpellId(), proc));
}

void Brawl::addWeaponProc(Weaponproc& proc, unsigned int spellId){
	weaponProcList.insert(std::pair<unsigned int, Weaponproc>(spellId, proc));
}