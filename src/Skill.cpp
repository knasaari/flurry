#include "Skill.h"
#include "functions.h"

Skill::Skill() {
	delay = 1000;
	step = 0;
	rageCost = 0;
	spellId = 0;
	flags = 0;
}

Autoattack::Autoattack(const Weapon &weapon, bool mainhand) : Skill() {
	this->weapon = weapon;
	this->mainhand = mainhand;
	spellId = 1;
	flags = WHITEATTACK | BENEFITS_FROM_WEAPONSKILL | SWING;
	delay = weapon.speed * 1000;
}

ExtraAutoattack::ExtraAutoattack(bool mainhand) : Autoattack(Weapon(), mainhand){
	flags = WHITEATTACK | BENEFITS_FROM_WEAPONSKILL;
	spellId = 3;
}

WindfuryAttack::WindfuryAttack(bool mainhand, bool enhancementShaman) : ExtraAutoattack(mainhand){
	spellId = 27621;
	extraAp = enhancementShaman?409:315; // 409 ap with Improved Weapon Totems 2/2. 315 ap basic
}

Whirlwind::Whirlwind() {
	delay = 10000;
	step = 0;
	rageCost = 25;
	spellId = 1680;
	flags = YELLOWATTACK | BENEFITS_FROM_WEAPONSKILL | GCD | IMPALE_MOD | MAINHAND;
}

Hamstring::Hamstring() {
	delay = 0;
	step = 0;
	rageCost = 10;
	spellId = 7373;
	flags = YELLOWATTACK | BENEFITS_FROM_WEAPONSKILL | GCD | IMPALE_MOD | MAINHAND;
}

Hamstring::Hamstring(int reducedCost) : Hamstring(){
	rageCost = std::max(rageCost - reducedCost, 0);
}

Bloodthirst::Bloodthirst() {
	delay = 6000;
	step = 0;
	rageCost = 30;
	spellId = 23894;
	flags = YELLOWATTACK | BENEFITS_FROM_WEAPONSKILL | GCD | IMPALE_MOD | MAINHAND;
}

Execute::Execute(){
	delay = 0;
	step = 0;
	rageCost = 15;
	spellId = 20662;
	flags = YELLOWATTACK | BENEFITS_FROM_WEAPONSKILL | GCD | IMPALE_MOD | MAINHAND | EXECUTE;
}

Execute::Execute(int improvedTalent) : Execute(){
	if(improvedTalent == 1){
		rageCost = 13;
	}
	else if(improvedTalent == 2){
		rageCost = 10;
	}
}

HeroicStrike::HeroicStrike() {
	delay = 0;
	step = 0;
	rageCost = 15;
	spellId = 11567;
	flags = YELLOWATTACK | BENEFITS_FROM_WEAPONSKILL | SWING | IMPALE_MOD | MAINHAND;
}

HeroicStrike::HeroicStrike(int improvedTalent) : HeroicStrike() {
	if(improvedTalent >= 1 && improvedTalent <= 3){
		rageCost -= improvedTalent;
	}
}

GlobalCD::GlobalCD() {
	delay = 1500;
	step = 0;
}

Flurry::Flurry() {
	delay = 15000;
	step = 0;
	count = 0;
	procCount = 0;
	spellId = 12974;
}

bool Skill::update(unsigned int step) {
	this->step += step;

	return this->ready();
}

bool Skill::ready() {
	return step >= delay;
}

void Skill::reset() {
	this->step = 0;
}

void Skill::reset(double mod) {
	reset();
}

void Skill::prime() {
	this->step = this->delay + 1000;
}

double Skill::getDamage(Weapon* a, int b){
	return 0;
}

int Skill::getRageCost() {
	return rageCost;
}

unsigned int Skill::getSpellId() const{
	return spellId;
}

void Skill::setSpellId(unsigned int spellId){
	this->spellId = spellId;
}

unsigned int Skill::getFlags(){
	return flags;
}

void Skill::setFlags(unsigned int flags){
	this->flags = flags;
}

unsigned int Skill::cooldownLeft(){
	return std::max(delay-step, (unsigned int)0);
}

double Autoattack::getDamage(Weapon *weapon, int ap) {
	return this->weapon.getDamage(this->mainhand, ap);
}

void Autoattack::reset() {
	delay = weapon.speed * 1000;
	step = 0;
}

void Autoattack::reset(double mod) {
	delay = (weapon.speed * 1000)/mod;
	step = 0;
}

double Autoattack::getSwingPct(){
	return (double)step/delay;
}

unsigned int Autoattack::getSwing(){
	return step;
}

unsigned int Autoattack::getSwingLeft(){
	return (step>delay)?0:delay - step;
}

void Autoattack::setSwingLeft(unsigned int millis){
	step = (millis>delay)?0:delay - millis;
}

double ExtraAutoattack::getDamage(Weapon* weapon, int ap){
	return weapon->getDamage(this->mainhand, ap);
}

bool ExtraAutoattack::update(unsigned int step) {
	return true;
}

bool ExtraAutoattack::ready(){
	return true;
}

double WindfuryAttack::getDamage(Weapon* weapon, int ap){
	return ExtraAutoattack::getDamage(weapon, ap + extraAp); // Basic Windfury rank 3
}

double Whirlwind::getDamage(Weapon *weapon, int ap) {
	return weapon->getDamage(true, ap, true);
}

double Hamstring::getDamage(Weapon *weapon, int ap) {
	return 45.0;
}

double Bloodthirst::getDamage(Weapon *weapon, int ap) {
	return (double) ap * 0.45;
}

double Execute::getDamage(Weapon *weapon, int rage) {
	return 600.0 + rage * 15.0;
}

double HeroicStrike::getDamage(Weapon *weapon, int ap) {
	return weapon->getDamage(true, ap) + 138;
}

bool Flurry::update(unsigned int step) {
	bool drop = Skill::update(step);
	if (drop) {
		this->count = 0;
	}
	return drop;
}

bool Flurry::isUp() {
	return this->count > 0;
}

void Flurry::reduce() {
	this->count--;
}

void Flurry::reset() {
	Skill::reset();
	this->count = 3;
	this->procCount++;
}

BRE::BRE(){
	delay = 10000;
	count = 0;
	procCount = 0;
}

bool BRE::update(unsigned int step){
	bool drop = Skill::update(step);
	if (drop) {
		this->count = 0;
	}
	return drop;
}

bool BRE::proc() {
	int roll = rng() % 10000;
	int chance = int(3.4 * 1000 * 2.6 / 6.0);
	bool procced = roll < chance;
	if (procced){
		procCount++;
		if(count < 3)
			count++;
		Skill::reset();
	}
	
	return procced;
}

int BRE::getCount(){
	return count;
}

bool BRE::isUp() {
	return this->count > 0;
}

void BRE::reset(){
	count = 0;
	Skill::reset();
}

GenericSkill::GenericSkill(unsigned int spellId, int low, int high, unsigned int flags){
	this->spellId = spellId;
	this->low = low;
	this->high = high;
	delay = 0;
	step = 0;
	rageCost = 0;
	this->flags = flags;
}

double GenericSkill::getDamage(Weapon* a, int b){
	std::uniform_int_distribution<int> distribution(low, high);
	return (double)distribution(rng);
}

Weaponproc::Weaponproc() {
	chance = 1;
	ppm = true;
	flags = PHYSICAL;
	procs = 0;
	multiproc = 0;
	skill = nullptr;
}

Weaponproc::Weaponproc(double chance, bool ppm, unsigned int flags, Skill *skill, std::string name, int multiproc) {
	this->name = name;
	this->chance = chance;
	this->ppm = ppm;
	this->flags = flags;
	this->skill = skill;
	this->multiproc = multiproc;
	this->procs = 0;
}

bool Weaponproc::roll(double speed) {
	int chance = 0;
	int roll = rng() % 10000;
	
	if(ppm)
		chance = std::round(speed * 1000 * this->chance / 6.0);
	else
		chance = this->chance*100.0;
	
	if(roll < chance){
		procs++;
		return true;
	}
	
	return false;
}

Skill* Weaponproc::getSkill() {
	return this->skill;
}

unsigned int Weaponproc::getFlags() {
	return flags;
}

int Weaponproc::getMultiproc() {
	return multiproc;
}

unsigned int Weaponproc::getProcs() {
	return procs;
}

void Weaponproc::setFlags(unsigned int flags) {
	this->flags = flags;;
}

void Weaponproc::addFlags(unsigned int flags) {
	this->flags |= flags;
}

std::string Weaponproc::getName(){
	return name;
}

GenericBuff::GenericBuff() : Skill(){
	uptimeTicks = 0;
}

GenericBuff::GenericBuff(unsigned int spellId, unsigned int flags, unsigned int delay, const Stats& stats) {
	this->spellId = spellId;
	step = 0;
	rageCost = 0;
	this->flags = flags;
	this->delay = delay;
	uptimeTicks = 0;
	this->stats = stats;
}

bool GenericBuff::update(unsigned int step) {
	bool dropped = Skill::update(step);
	
	if(!dropped)
		uptimeTicks++;
	
	return dropped;
}

unsigned int GenericBuff::getUptime() const {
	return uptimeTicks;
}

Stats GenericBuff::getStats(){
	return stats;
}
