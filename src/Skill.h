#ifndef SKILL_H
#define SKILL_H

#include "Weapon.h"

class Skill {
protected:
	unsigned int delay, step;
	int rageCost;
	unsigned int spellId, flags;

public:
	Skill();
	
	virtual bool update(unsigned int);
	virtual bool ready();
	virtual void reset();
	virtual void reset(double);
	void prime();
	virtual double getDamage(Weapon*, int);
	
	int getRageCost();
	unsigned int getSpellId() const;
	void setSpellId(unsigned int);
	unsigned int getFlags();
	void setFlags(unsigned int);
	unsigned int cooldownLeft();
};

class Weaponproc {
	std::string name;
	double chance;
	bool ppm;
	int multiproc;
	unsigned int flags, procs;
	Skill *skill;

public:
	Weaponproc();
	Weaponproc(double, bool, unsigned int, Skill*, std::string = "", int = 0);
	
	bool roll(double);
	Skill* getSkill();
	unsigned int getFlags();
	int getMultiproc();
	unsigned int getProcs();
	void setFlags(unsigned int);
	void addFlags(unsigned int);
	std::string getName();
};

class Autoattack : public Skill {
private:
	Weapon weapon;
protected:
	bool mainhand;
public:
	double getDamage(Weapon*, int);
	void reset();
	void reset(double);
	double getSwingPct();
	unsigned int getSwing();
	unsigned int getSwingLeft();
	void setSwingLeft(unsigned int);
	Autoattack(const Weapon&, bool);
};

class ExtraAutoattack : public Autoattack {
public:
	virtual double getDamage(Weapon*, int);
	bool update(unsigned int);
	bool ready();
	ExtraAutoattack(bool);
};

class WindfuryAttack : public ExtraAutoattack {
private:
	int extraAp;
public:
	double getDamage(Weapon*, int);
	WindfuryAttack(bool, bool);
};

class Whirlwind : public Skill {
public:
	double getDamage(Weapon*, int);

	Whirlwind();
};

class Hamstring : public Skill {
public:
	double getDamage(Weapon*, int);

	Hamstring();
	Hamstring(int);
};

class Bloodthirst : public Skill {
public:
	double getDamage(Weapon*, int);

	Bloodthirst();
};

class Execute : public Skill {
public:
	double getDamage(Weapon*, int);

	Execute();
	Execute(int);
};

class HeroicStrike : public Skill {
public:
	double getDamage(Weapon*, int);

	HeroicStrike();
	HeroicStrike(int);
};

class GlobalCD : public Skill {
public:
	GlobalCD();
};

class Flurry : public Skill {
private:
	int count;
public:
	int procCount;

	bool update(unsigned int);
	bool isUp();
	void reduce();
	void reset();
	Flurry();
};

class BRE : public Skill {
private:
	int count;
public:
	int procCount;
	
	bool update(unsigned int);
	bool proc();
	bool isUp();
	int getCount();
	void reset();
	BRE();
};

class GenericSkill : public Skill {
private:
	int low, high;
public:
	double getDamage(Weapon*, int);
	GenericSkill(unsigned int, int, int, unsigned int);
};

class GenericBuff : public Skill {
private:
	Stats stats;
	unsigned int uptimeTicks;
	
public:
	GenericBuff();
	GenericBuff(unsigned int, unsigned int, unsigned int, const Stats&);
	
	bool update(unsigned int);
	unsigned int getUptime() const;
	Stats getStats();
};
#endif /* SKILL_H */

