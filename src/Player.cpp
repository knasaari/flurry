#include "Player.h"
#include "functions.h"
#include <stdexcept>

const std::map<std::string, std::pair<int,int>> Player::baseStats = {
	{"Orc", {Agi::ORC, Str::ORC}},
	{"Undead", {Agi::UNDEAD, Str::UNDEAD}},
	{"Tauren", {Agi::TAUREN, Str::TAUREN}},
	{"Troll", {Agi::TROLL, Str::TROLL}}
};

Player::Player() : stats(Str::ORC, Agi::ORC, AP, 0.0, 0.0, 0, 0.0) {
	name = "Default template";
	level = 60;
	race = "Orc";
}

void Player::fromData(std::string name, std::string race, int str, int agi, int ap, int critChance, int hitChance, int weaponskill){
	int baseStr = 0, baseAgi = 0;
	std::map<std::string, std::pair<int,int>>::const_iterator it = baseStats.find(race);
	if(it == baseStats.end()){
		throw std::invalid_argument("Player " + name + " has undefined race '" + race + "'");
	}
	
	baseAgi = it->second.first;
	baseStr = it->second.second;
	
	this->race = it->first;
	this->name = name;
	stats.str = str + baseStr; 
	stats.agi = agi + baseAgi;
	stats.ap = AP + ap;
	stats.critChance = critChance; // 3% Berserker stance
	stats.hitChance = hitChance;
	stats.weaponskill = weaponskill;
}

int Player::totalAp() const{
	return stats.str * 2 + stats.ap;
}

double Player::totalCrit() const{
	return agiToCrit(stats.agi) + stats.critChance;
}

Player Player::operator+(const Item &item) const{
	Player output = *this;
	output.stats = output.stats + item.stats + item.getSetBonuses();
	
	return output;
}

Player Player::operator-(const Item &item) const{
	Player output = *this;
	output.stats = output.stats - item.stats;
	
	return output;
}

std::ostream& operator<<(std::ostream& stream, const Player& p){
	stream << 
			p.name << " - " <<
			"lvl " << p.level << " " << p.race << ". " <<
			"str " << p.stats.str << ", " <<
			"agi " << p.stats.agi << ", " <<
			"ap " << p.totalAp() << ", " <<
			"crit " << p.totalCrit() << "%, " <<
			"hit " << p.stats.hitChance << "%, " <<
			"weaponskill " << p.stats.weaponskill;
	return stream;
}

void from_json(const nlohmann::json& j, Player& p) {
	Player temp;
	int str = j.at("str");
	int agi = j.at("agi");
	int ap = j.at("ap");
	std::string race = j.at("race");
	double critChance = j.at("crit");
	double hitChance = j.at("hit");
	double weaponskill = j.at("weaponskill");
	std::string name = j.at("name");
	temp.fromData(name, race, str, agi, ap, critChance, hitChance, weaponskill);
	p = temp;
}

Target::Target(){
	dodgeChance = 6.5;
	parryChance = 0;
	blockChance = 0;
	level = 63;
	health = 150000;
	armor = 3731;		// Usual boss armor
	resistance = 15;	// Seems MC/BWL/ZG bosses use only 15 resistance
}

Buffs::Buffs(){
	mongoose = false;
	blessedSunFruit = false;
	sandwormMeat = false;
	giants = false;
	
	faerieFire = false;
	curseOfRecklessness = false;
	annihilator = false;
	
	enhaInGroup = false;
	windfuryTotem = false;
	strTotem = false;
	agiTotem = false;
	motw = false;
	trueshotAura = false;
	
	onyBuff = false;
	zgBuff = false;
	dmAPBuff = false;
	rendBuff = false;
	songflowerBuff = false;
}


void from_json(const nlohmann::json& j, Buffs& b) {
	Buffs temp;
	
	temp.mongoose = j.at("mongoose");
	temp.blessedSunFruit = j.at("blessedSunFruit");
	temp.sandwormMeat = j.at("sandwormMeat");
	temp.giants = j.at("giants");
	
	temp.faerieFire = j.at("faerieFire");
	temp.curseOfRecklessness = j.at("curseOfRecklessness");
	temp.annihilator = j.at("annihilator");
	
	temp.enhaInGroup = j.at("enhaInGroup");
	temp.windfuryTotem = j.at("windfuryTotem");
	temp.strTotem = j.at("strTotem");
	temp.agiTotem = j.at("agiTotem");
	temp.motw = j.at("motw");
	temp.trueshotAura = j.at("trueshotAura");
	
	temp.onyBuff = j.at("onyBuff");
	temp.zgBuff = j.at("zgBuff");
	temp.dmAPBuff = j.at("dmAPBuff");
	temp.rendBuff = j.at("rendBuff");
	temp.songflowerBuff = j.at("songflowerBuff");
	
	b = temp;
}

Talents::Talents() {
	improvedBattleShout = 0;
	improvedExecute = 0;
	improvedHeroicStrike = 0;
	unbridledWrath = 0;
	impale = 0;
	cruelty = 0;
}

void from_json(const nlohmann::json& j, Talents& t) {
	Talents temp;
	
	temp.improvedBattleShout = j.at("improvedBattleShout");
	temp.improvedExecute = j.at("improvedExecute");
	temp.improvedHeroicStrike = j.at("improvedHeroicStrike");
	temp.unbridledWrath = j.at("unbridledWrath");
	temp.impale = j.at("impale");
	temp.cruelty = j.at("cruelty");
	
	t = temp;
}

Options::Options() {
	tanking = false;
	use_hamstring = false;
	RageofMugamba = false;
	BloodGuardsPlateGloves = false;
	HandofJustice = false;
	DarkmoonCardMaelstrom = false;
	hamstring_swing_limit = 0.5;
	HeroicStrikeMinRage = 50;
}


void from_json(const nlohmann::json& j, Options& o) {
	Options temp;
	temp.tanking = j.at("tanking");
	temp.use_hamstring = j.at("use_hamstring");
	temp.HeroicStrikeMinRage = j.at("HeroicStrikeMinRage");
	temp.hamstring_swing_limit = j.at("hamstring_swing_limit");

	o = temp;
}
