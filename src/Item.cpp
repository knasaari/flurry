#include "Item.h"
#include "functions.h"
#include "json_functions.h"

Stats::Stats(){
	str = 0;
	agi = 0;
	ap = 0;
	critChance = 0;
	hitChance = 0;
	weaponskill = 0;
	spellCritChance = 0;
	armorPenetration = 0;
}

Stats::Stats(int str, int agi, int ap, double crit, double hit, int weaponskill, double spell, int armorPenetration) 
		: str(str), agi(agi), ap(ap), critChance(crit), hitChance(hit), weaponskill(weaponskill), spellCritChance(spell), armorPenetration(armorPenetration){

}

Stats& Stats::operator+=(const Stats &stats){
	str += stats.str;
	agi += stats.agi;
	ap += stats.ap;
	critChance += stats.critChance;
	hitChance += stats.hitChance;
	weaponskill += stats.weaponskill;
	spellCritChance += stats.spellCritChance;
	armorPenetration += stats.armorPenetration;
	return *this;
}

Stats Stats::operator+(const Stats &stats) const{
	return Stats(str+stats.str, agi+stats.agi, ap+stats.ap, critChance+stats.critChance, hitChance+stats.hitChance, weaponskill+stats.weaponskill, spellCritChance+stats.spellCritChance, armorPenetration+stats.armorPenetration);
}

Stats Stats::operator-(const Stats &stats) const{
	return Stats(str-stats.str, agi-stats.agi, ap-stats.ap, critChance-stats.critChance, hitChance-stats.hitChance, weaponskill-stats.weaponskill, spellCritChance-stats.spellCritChance, armorPenetration-stats.armorPenetration);
}

Stats Stats::operator -() const{
	return Stats(-str, -agi, -ap, -critChance, -hitChance, -weaponskill, -spellCritChance);
}

std::ostream& operator<<(std::ostream& stream, const Stats& stats){
	stream << 
		"Str:\t" << stats.str << "\n" <<
		"Agi:\t" << stats.agi << "\n" <<
		"Ap:\t" << stats.ap << "\n" <<
		"Crit:\t" << stats.critChance << "\n" <<
		"Hit:\t" << stats.hitChance << "\n" <<
		"WSkill:\t" << stats.weaponskill << "\n" <<
		"Arp:\t" << stats.armorPenetration;
	return stream;
}

void from_json(const nlohmann::json& j, Stats& stats){
	Stats temp;
	temp.str = j.at("str");
	temp.agi = j.at("agi");
	temp.ap = j.at("ap");
	temp.critChance = j.at("crit");
	temp.hitChance = j.at("hit");
	temp.weaponskill = j.at("weaponskill");
	temp.spellCritChance = j.at("scrit");
	stats = temp;
}

void to_json(nlohmann::json& j, const Stats& stats){
	nlohmann::json temp;
	temp["str"] = stats.str;
	temp["agi"] = stats.agi;
	temp["ap"] = stats.ap;
	temp["crit"] = stats.critChance;
	temp["hit"] = stats.hitChance;
	temp["weaponskill"] = stats.weaponskill;
	temp["scrit"] = stats.spellCritChance;
	
	j = temp;
}


int Stats::totalAp() const{
	return str * 2 + ap;
}

double Stats::totalCrit() const{
	return agiToCrit(agi) + critChance;
}

Item::Item(){
	name = "Unnamed Item";
}

Item::Item(const std::string  &name, const Stats &stats){
	this->name = name;
	this->stats = stats;
}

Item::Item(int str, int agi, int ap, double crit, double hit, int weaponskill, double spellCritChance, int armorPenetration)
: stats(str,agi,ap,crit,hit,weaponskill,spellCritChance,armorPenetration) {
}

void Item::addSet(const Set& set, int n){
	std::map<Set,int>::iterator it = sets.find(set);
	if(it == sets.end()){
		sets.insert(std::pair<Set,int>(set, 1));
	}
	else
		it->second += n;
}

void Item::removeSet(const Set& set, int n){
	std::map<Set,int>::iterator it = sets.find(set);
	if(it != sets.end()){
		if(it->second <= n ){
			sets.erase(it);
		}
		else{
			it->second -= n;
		}
	}
}

Stats Item::getSetBonuses() const{
	Stats stats;
	std::map<Set,int>::const_iterator it;
	for(it=sets.begin(); it!=sets.end(); ++it){
		stats += it->first.getStats(it->second);
	}
	return stats;
}

bool Item::hasSetBonuses() const {
	return !sets.empty();
}

Item& Item::operator+=(const Item &other){
	name.append("+" + other.name);
	stats += other.stats;
	
	std::map<Set,int>::const_iterator it;
	std::map<Set,int>::const_iterator end = other.sets.end();
	for(it=other.sets.begin(); it!=end; ++it){
		addSet(it->first, it->second);
	}
	return *this;
}

Item Item::operator+(const Item &other) const{
	Item combined(name + "+" + other.name, stats + other.stats);
	combined.sets = sets;
	
	std::map<Set,int>::const_iterator it;
	std::map<Set,int>::const_iterator end = other.sets.end();
	for(it=other.sets.begin(); it!=end; ++it){
		combined.addSet(it->first, it->second);
	}
	return combined;
}

Item Item::operator-(const Item &other) const{
	Item combined(name + "-" + other.name, stats - other.stats);
	combined.sets = sets;
	
	std::map<Set,int>::const_iterator it;
	std::map<Set,int>::const_iterator end = other.sets.end();
	for(it=other.sets.begin(); it!=end; ++it){
		combined.addSet(it->first, -(it->second));
	}
	return combined;
}

Item Item::operator-() const{
	Item combined("-" + name, -stats);
	combined.sets = sets;
	std::map<Set,int>::iterator it;
	std::map<Set,int>::iterator end = combined.sets.end();
	for(it=combined.sets.begin(); it!=end; ++it){
		it->second = -(it->second);
	}
	return combined;
}

bool Item::operator<(const Item &item) const {
	return name < item.name;
}

bool Item::operator()(const Item &a, const Item &b) const {
	return a.name < b.name;
}

bool Item::operator()(const Item &item, const std::string &str) const {
	return item.name < str;
}

bool Item::operator()(const std::string &str, const Item &item) const {
	return str < item.name;
}

std::ostream& operator<<(std::ostream& stream, const Item& item){
	stream << 
			item.name << ":\n" <<
			item.stats;
	if(item.hasSetBonuses()){
		stream << "\n\nSet bonuses:\n" << item.getSetBonuses();
	}
	return stream;
}

void to_json(nlohmann::json& j, const Item& t) {
	nlohmann::json temp = {{"name", t.name}, {"str", t.stats.str}, {"agi", t.stats.agi}, {"ap", t.stats.ap}, {"crit", t.stats.critChance},
	{"hit", t.stats.hitChance}, {"weaponskill", t.stats.weaponskill}, {"scrit", t.stats.spellCritChance}};
	
	for(std::map<Set,int>::const_iterator it = t.sets.begin(); it!=t.sets.end();++it){
		temp["sets"].push_back(it->first);
	}
	
	j = temp;
}

void from_json(const nlohmann::json& j, Item& t) {
	Item temp;
	
	assign_if_exists(temp.name, j, "name");
	assign_if_exists(temp.stats.str, j, "str");
	assign_if_exists(temp.stats.agi, j, "agi");
	assign_if_exists(temp.stats.ap, j, "ap");
	assign_if_exists(temp.stats.critChance, j, "crit");
	assign_if_exists(temp.stats.hitChance, j, "hit");
	assign_if_exists(temp.stats.weaponskill, j, "weaponskill");
	assign_if_exists(temp.stats.spellCritChance, j, "scrit");
	assign_if_exists(temp.stats.armorPenetration, j, "arp");
	if (j.find("sets") != j.end()) {
		for (nlohmann::json::const_iterator it = j["sets"].begin(); it != j["sets"].end(); ++it) {
			Set set = *it;
			temp.addSet(set);
		}
	}
	t = temp;
}

Set::Set(){
	this->name = "Unknown Set";
	this->id = 0;
}

Set::Set(const std::string& name, unsigned int id){
	this->name = name;
	this->id = id;
}

void Set::addBonus(int items, const Stats& stats){
	bonuses.push_back(std::pair<int, Stats>(items, stats));
}

Stats Set::getStats(int items) const{
	Stats stats;
	for(int i=0;i<bonuses.size();i++){
		
		if(bonuses[i].first <= items)
			stats = stats + bonuses[i].second;
	}
	return stats;
}

bool Set::operator<(const Set &other) const {
	return id < other.id;
}

void from_json(const nlohmann::json& j, Set& set){
	Set temp(j.at("name"),j.at("id"));
	for (nlohmann::json::const_iterator it = j.at("bonuses").begin(); it != j.at("bonuses").end(); ++it) {
		std::pair<int, Stats> s = *it;
		temp.addBonus(s.first, s.second);
	}
	set = temp;
}

void to_json(nlohmann::json& j, const Set& set){
	nlohmann::json temp;
	temp["name"] = set.name;
	temp["id"] = set.id;
	temp["bonuses"] = set.bonuses;
	j = temp;
}

Outfit::Outfit(){
	name = "Unknown outfit";
}

void from_json(const nlohmann::json& j, Outfit& outfit){
	Outfit temp;
	assign_if_exists(temp.name, j, "name");
	std::vector<std::string> items = j.at("items");
	temp.items = items;
	outfit = temp;
}