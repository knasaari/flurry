#include "Rage.h"

Rage::Rage() {
	value = 0;
	
	wasted = 0;
	used = 0;
	gained = 0;
}

void Rage::add(double rage){
	double newRage = value + rage;
	if(newRage > 100.0){
		wasted += newRage - 100.0;
		newRage = 100.0;
	}
	gained += rage;
	value = newRage;
}

void Rage::refund(double rage){
	double newRage = value + rage;
	if(newRage > 100.0){
		wasted += newRage - 100.0;
		newRage = 100.0;
	}
	used -= rage;
	value = newRage;
}

void Rage::spend(int rage){
	used += (rage > value)?value:rage;
	value -= rage;
	
	if(value < 0.0){
		value = 0.0;
	}
}

void Rage::reset(){
	value = 0;
}

double Rage::current(){
	return value;
}