#ifndef OUTCOME_H
#define OUTCOME_H

#include <string>
#include <sstream>
#include <unordered_map>
#include <cstdint>
#include "functions.h"

class DamageCount {
private:
public:
	uint64_t damage;
	unsigned int count, max, min;
	DamageCount();
	DamageCount(unsigned int damage, unsigned int count, unsigned int min, unsigned int max);
	
	void add(unsigned int damage);
	double percentage(int casts);
	double average() const;
	
	void operator+=(const DamageCount&);
	friend DamageCount operator+(const DamageCount &a, const DamageCount &b);
};

class Outcome {
private:
	static const unsigned int hitFilter = HIT | CRIT | GLANCING | MISS | PARRY | DODGE | BLOCK | RESIST;

	std::string spellName;
	unsigned int casts;	
	std::unordered_map<unsigned int, DamageCount> hitType;
	std::unordered_map<unsigned int, std::string> logAction;
	
public:
	Outcome();
	Outcome(const std::string spellName);
	
	void setSpellName(std::string spellName);
	void add(unsigned int type, unsigned int damage);
	void output(std::stringstream &os, unsigned int type, int damage, int i);
	std::string header();
	std::string damageInfo(int simulations, double seconds, bool header = false) const;
	double totalDPS(double seconds);
	
	friend std::ostream& operator<< (std::ostream& stream, Outcome& oc);
	void operator+=(const Outcome &other);
	Outcome operator+(const Outcome &other) const;
};

class Simulation {
public:
	double seconds;
	Outcome data;
	
	Simulation();
	Simulation(double, const Outcome&);
};

#endif /* OUTCOME_H */

