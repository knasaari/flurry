#include "json_functions.h"

void assign_if_exists(std::string &value, const nlohmann::json &json_object, const std::string& key) {
	if (json_object.find(key) != json_object.end()) {
		value = json_object.at(key);
	}
}

void assign_if_exists(int &value, const nlohmann::json &json_object, const std::string& key) {
	if (json_object.find(key) != json_object.end()) {
		value = json_object.at(key);
	}
}

void assign_if_exists(double &value, const nlohmann::json &json_object, const std::string& key) {
	if (json_object.find(key) != json_object.end()) {
		value = json_object.at(key);
	}
}
