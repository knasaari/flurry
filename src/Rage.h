#ifndef RAGE_H
#define RAGE_H

class Rage {
public:
	Rage();
	void spend(int);
	void add(double);
	void refund(double);
	void reset();
	double current();
	
	double value;
	double wasted, gained, used;
};

#endif /* RAGE_H */

