#include "Outcome.h"
#include "functions.h"
#include <iomanip>

DamageCount::DamageCount(){
	damage = 0;
	count = 0;
	min = 0;
	max = 0;
}

DamageCount::DamageCount(unsigned int damage, unsigned int count, unsigned int min, unsigned int max) : damage(damage), count(count), min(min), max(max){
}

void DamageCount::add(unsigned int damage){
	count++;
	if(damage > 0){
		this->damage += damage;

		if(damage > max){
			max = damage;
		}
		if(damage < min || damage > 0 && min == 0)
			min = damage;
	}
}

double DamageCount::percentage(int casts){
	if(casts == 0)
		return 0.0;

	return (double)count / casts * 100.0;
}

double DamageCount::average() const{
	if(count == 0)
		return 0;

	return damage/count;
}

DamageCount operator+(const DamageCount &a, const DamageCount &b){
	return DamageCount(a.damage + b.damage, a.count + b.count, std::min(a.min, b.min), std::max(a.max, b.max));
}

void DamageCount::operator+=(const DamageCount &b){
	this->damage += b.damage;
	this->count += b.count;
	this->min = std::min(this->min, b.min);
	this->max = std::max(this->max, b.max);
}


Outcome::Outcome(){
	casts = 0;
	
	hitType.insert(std::pair<unsigned int, DamageCount>(HIT, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(CRIT, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(GLANCING, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(MISS, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(PARRY, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(DODGE, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(BLOCK, DamageCount()));
	hitType.insert(std::pair<unsigned int, DamageCount>(RESIST, DamageCount()));	

	logAction.insert(std::pair<unsigned int, std::string>(HIT, "hits for"));
	logAction.insert(std::pair<unsigned int, std::string>(CRIT, "crits for"));
	logAction.insert(std::pair<unsigned int, std::string>(GLANCING, "glances for"));
	logAction.insert(std::pair<unsigned int, std::string>(MISS, "miss"));
	logAction.insert(std::pair<unsigned int, std::string>(PARRY, "parry"));
	logAction.insert(std::pair<unsigned int, std::string>(DODGE, "dodge"));
	logAction.insert(std::pair<unsigned int, std::string>(BLOCK, "block"));
	logAction.insert(std::pair<unsigned int, std::string>(RESIST, "resist"));
}
	
Outcome::Outcome(const std::string spellName):Outcome() {
	this->spellName = spellName;
}

void Outcome::setSpellName(std::string spellName){
	this->spellName = spellName;
}

void Outcome::add(unsigned int type, unsigned int damage){
	casts++;
	hitType[type & hitFilter].add(damage);
}

void Outcome::output(std::stringstream &os, unsigned int type, int damage, int i){
	int fraction = i % 50;
	double fractionTime = fraction * (1.0/50.0);
	double second = (i - fraction)/50 + fractionTime;
	os << second << " " << spellName << " " << logAction[type & hitFilter];
	if(damage > 0)
		os << " " << damage;
	os << std::endl;
}

std::string Outcome::header(){
	return std::string("Skill\tHit\tCrit\tGlncg.\tMiss\tParry.\tDodge\tBlock\tResist\n---------------------------------------------------------------------");
}

std::string Outcome::damageInfo(int simulations, double seconds, bool header) const{
	uint64_t totalDamage = 0, totalHits = 0;
	for (std::unordered_map<unsigned int, DamageCount>::const_iterator it=hitType.begin(); it!=hitType.end(); ++it){
		if(it->first & CONNECTED){
			totalDamage += it->second.damage;
			totalHits += it->second.count;
		}
	}
	std::ostringstream os;
	if(header)
		os << "Skill\tDPS\tCasts\tHits\tTotal\tAvg\tLow.h\tAvg.h\tHigh.h\tLow.c\tAvg.c\tHigh.c" << std::endl <<
		"--------------------------------------------------------------------------------------------" << std::endl;

	unsigned int averageDamage = (totalHits > 0) ? std::round((double) totalDamage / totalHits) : 0;
	os << std::setprecision(1) << std::fixed <<
			spellName << "\t" <<
			(double) totalDamage / seconds << "\t" <<
			(double) casts / simulations << "\t" <<
			(double) totalHits / simulations << "\t" <<
			std::round( totalDamage / simulations) << "\t" <<
			averageDamage << "\t" << std::setprecision(0) <<
			hitType.at(HIT).min << "\t" <<
			hitType.at(HIT).average() << "\t" <<
			hitType.at(HIT).max << "\t" <<
			hitType.at(CRIT).min << "\t" <<
			hitType.at(CRIT).average() << "\t" <<
			hitType.at(CRIT).max;
	return os.str();
}

double Outcome::totalDPS(double seconds){
	if(seconds == 0)
		return 0;
	
	int totalDamage = 0;
	for (std::unordered_map<unsigned int, DamageCount>::iterator it=hitType.begin(); it!=hitType.end(); ++it){
		if(it->first & CONNECTED){
			totalDamage += it->second.damage;
		}
	}
	
	return (double) totalDamage / seconds;
}

std::ostream& operator<<(std::ostream& stream, Outcome& oc){
	stream << 
			oc.spellName << "\t" <<
			oc.hitType[HIT].percentage(oc.casts) << "%\t" <<
			oc.hitType[CRIT].percentage(oc.casts) << "%\t" <<
			oc.hitType[GLANCING].percentage(oc.casts) << "%\t" <<
			oc.hitType[MISS].percentage(oc.casts)  << "%\t" <<
			oc.hitType[PARRY].percentage(oc.casts) << "%\t" <<
			oc.hitType[DODGE].percentage(oc.casts) << "%\t" <<
			oc.hitType[BLOCK].percentage(oc.casts) << "%\t" <<
			oc.hitType[RESIST].percentage(oc.casts) << "%";
	return stream;
}

void Outcome::operator+=(const Outcome &other) {
	this->setSpellName(spellName + "+" + other.spellName);
	this->casts += other.casts;

	for (std::unordered_map<unsigned int, DamageCount>::iterator it=hitType.begin(); it!=hitType.end(); ++it){
		it->second += other.hitType.at(it->first);
	}
}

Outcome Outcome::operator+(const Outcome &other) const{
	Outcome combined(this->spellName + "+" + other.spellName);
	combined.casts = this->casts + other.casts;

	for (std::unordered_map<unsigned int, DamageCount>::iterator it=combined.hitType.begin(); it!=combined.hitType.end(); ++it){
		combined.hitType[it->first] = this->hitType.at(it->first) + other.hitType.at(it->first);
	}

	return combined;
}

Simulation::Simulation() {
	seconds = 1;
}

Simulation::Simulation(double seconds, const Outcome& data) {
	this->seconds = seconds;
	this->data = data;
}
