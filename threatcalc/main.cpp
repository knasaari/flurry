#include <iostream>
#include <sstream>
#include <cstring>
#include <random>
#include <chrono>
#include "../src/functions.h"
#include "../src/Weapon.h"

std::mt19937 rng;

int main(int argc, char** argv) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	rng = std::mt19937(seed);
	
	//Weapon weapon("Crul'shoruk", 2.3, 101, 188, 0, 0, 36, 0, 0, 0);
	//Weapon weapon("Perdition's Blade", 1.8, 73, 137, 0, 0, 0, 0, 0, 0);
	Weapon weapon("Finkle's Skinner", 1.3, 37, 70, 0, 0, 0, 0, 0, 0);
	
	int ap = 1000;
	double threatMultiplier = 1.15*1.3;
	double impaleMultiplier = 1.2;
	int swingDamage = weapon.getAverageDamage(true, ap);
	int blockValue = 189;
	int shieldSlamDamage = (342+359)/2;
	int revengeDamage = (64+79)/2;
	double swingSpeed = weapon.speed;
	bool rendBuff = true;
	swingSpeed = (rendBuff)?1.0/1.15*swingSpeed:swingSpeed;
	
	int bossArmor = 4691;
	int totalArmor = bossArmor;
	totalArmor -= 450*5;
	totalArmor -= 505;
	totalArmor -= 640;
	totalArmor = (totalArmor < 0)?0:totalArmor;
	
	int sunderThreat = 261;
	int heroicThreat = 145;
	int heroicExtraDamage = 138;
	int shieldSlamThreat = 250;
	int revengeThreat = 315;
	
	double critchance = 0.2;
	
	int heroicDamage = swingDamage + heroicExtraDamage;
	
	int heroicAverageDamage = heroicDamage * critchance * impaleMultiplier + heroicDamage;
	heroicAverageDamage = getArmorReducedDamage(totalArmor, heroicAverageDamage, 60);
	
	int swingAverageDamage = swingDamage * critchance + swingDamage * 0.6 + swingDamage * 0.4 * 0.85;
	swingAverageDamage = getArmorReducedDamage(totalArmor, swingAverageDamage, 60);
	
	int shieldSlamAverageDamage = (shieldSlamDamage+blockValue);
	shieldSlamAverageDamage = shieldSlamAverageDamage * critchance * impaleMultiplier + shieldSlamAverageDamage;
	shieldSlamAverageDamage = getArmorReducedDamage(totalArmor, shieldSlamAverageDamage, 60);
	
	int revengeAverageDamage = getArmorReducedDamage(totalArmor, revengeDamage * critchance * impaleMultiplier + revengeDamage, 60);
	
	int heroicRageCost = 12;
	int sunderRageCost = 12	;
	int shieldSlamRageCost = 20;
	int revengeRageCost = 5;
	
	int sunderTotalThreat = sunderThreat * threatMultiplier;
	int averageSwingThreat = swingAverageDamage * threatMultiplier;
	int averageHeroicThreat = (heroicAverageDamage + heroicThreat) * threatMultiplier;
	int averageShieldSlamThreat = (shieldSlamAverageDamage + shieldSlamThreat) * threatMultiplier;
	int averageRevengeThreat = (revengeAverageDamage + revengeThreat) * threatMultiplier;
	
	int hitCount = 20 + 10 + 60.0/swingSpeed;
	int windfuryProcs = hitCount/5;
	int vaelThreat = 20 * sunderTotalThreat + 10 * averageRevengeThreat + 10 * averageShieldSlamThreat + (60.0/swingSpeed * averageHeroicThreat) + windfuryProcs * averageSwingThreat;
	
	std::cout << 1.7/(((1.0/60.0)*(1.0/0.85))/1.7) << std::endl;
	std::cout << 
	"Raw swing: " << swingDamage << std::endl <<
	"Swing average: " << swingAverageDamage << std::endl <<
	"HS average: " << heroicAverageDamage << std::endl <<
	"SS average: " << shieldSlamAverageDamage << std::endl <<
	"Revenge average: " << revengeAverageDamage << std::endl <<
	"Swing rage gain: " << rageGained(swingAverageDamage, 60) << std::endl <<
	
	"Boss armor: " << totalArmor << std::endl <<
	"---------------" << std::endl <<
	"Sunder threat: " << sunderTotalThreat << ", tpr: " << sunderTotalThreat/sunderRageCost << std::endl <<
	"Average swing threat: " << averageSwingThreat << std::endl <<
	"Average sunder + swing threat: " << averageSwingThreat + sunderTotalThreat << ", tpr: " << (sunderTotalThreat + averageSwingThreat)/sunderRageCost << std::endl <<
	"Average heroic threat: " << averageHeroicThreat << ", tpr: " << averageHeroicThreat/(heroicRageCost + rageGained(swingAverageDamage, 60)) << std::endl <<
	"Average shield slam threat: " << averageShieldSlamThreat << ", tpr: " << averageShieldSlamThreat/shieldSlamRageCost << std::endl <<
	"Average revenge threat: " << averageRevengeThreat << ", tpr: " << averageRevengeThreat/revengeRageCost << std::endl <<
	"---------------" << std::endl <<
	"60 second tps HS: " << 60.0/swingSpeed * averageHeroicThreat / 60.0 << std::endl <<
	"60 second tps sunder: " << (60.0/1.5 * sunderTotalThreat + 60.0/swingSpeed * averageSwingThreat) / 60.0 << std::endl <<
	"60 second Vael: " << vaelThreat << ", tps: "<< vaelThreat / 60.0 << ", wf procs: " << windfuryProcs << ", wf dmg: " << windfuryProcs*swingAverageDamage << std::endl <<
	std::endl;
	return 0;
}
