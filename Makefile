CC=g++
BIN=bin
SOURCE=src
GIT_VERSION := $(shell git describe --abbrev=4 --always --tags)
DATE := \"$(GIT_VERSION) $(shell date +%D+%X)\"
override CFLAGS += -std=c++14 -O2 -DVERSION="$(DATE)" -I./include
SYS := $(shell $(CC) -dumpmachine)
ifneq (, $(findstring linux, $(SYS)))
   FIND_TOOL = find
else ifneq (, $(findstring mingw, $(SYS)))
   FIND_TOOL = /bin/find
endif
OBS = $(shell $(FIND_TOOL) ./$(SOURCE) -type f -name "*.cpp" |sed -e "s/.cpp/.o/g" -e "s/.\/$(SOURCE)/.\/$(BIN)/g" |tr "\\n" " ")
WINRES = proj.res

all: windows

%.res: %.rc
	windres $< -O coff -o $@

windows: $(OBS) $(WINRES)
	$(CC) $(OBS) proj.res -o flurry.exe

windowsstatic: $(OBS) $(WINRES)
	$(CC) $(OBS) -static-libgcc -static-libstdc++ proj.res -o flurry.exe

windowsmini:
	$(CC) src/Rage.cpp src/Brawl.cpp src/Item.cpp src/Player.cpp src/Skill.cpp src/Weapon.cpp src/functions.cpp src/main.cpp -I./include $(CFLAGS) proj.res -o flurry.exe

linux: $(OBS)
	$(CC) $(OBS) -lpthread -no-pie -o flurry

linuxmini:
	$(CC) src/Rage.cpp src/Brawl.cpp src/Item.cpp src/Player.cpp src/Skill.cpp src/Weapon.cpp src/functions.cpp src/main.cpp -lpthread -no-pie -I./include $(CFLAGS) -o flurry

$(BIN)/%.o: $(SOURCE)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@echo $(OBS) $(WINRES) |xargs rm -f
